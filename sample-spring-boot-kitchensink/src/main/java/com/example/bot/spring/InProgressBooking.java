package com.example.bot.spring;

import java.util.Calendar;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
/** A class to hold details of the in progress booking */
@Slf4j
public class InProgressBooking {
	/** currently booking tour */
	private Tour tour;
	/** Line ID of the customer */
	private String lineID;
	/** Date of going */
	private Date date;
	/** Number of adults going */
	private int noOfAdults;
	/** Number of children going */
	private int noOfChildren;
	/** Number of toodlers going */
	private int noOfToddler;
	/** tour fee */
	private double tourFee;
	/**Total Fee of the tour (including tour fee and service charge
	 * @see #price*/
	private double totalFee;
	/**Special Request of the customer*/
	private String specialRequest;
	/**To search for the correct capacity corresponding to the date*/
	private int indexCapacity;

	/** Constructor. It sets index of capacity to be 0. All other are either set
	 * to null or -1*/
	public InProgressBooking() {
		this.tour = null;
		this.lineID = null;
		this.date = null;
		this.noOfAdults = -1;
		this.noOfChildren = -1;
		this.noOfToddler = -1;
		this.tourFee = -1.0;
		this.totalFee = -1.0;
		this.specialRequest = null;
		this.indexCapacity = 0;
	}

	/** Get currenly booking tour
	 * @return currenly booking tour
	 * @see #tour */
	public Tour getTour() { return tour; }
	/** Get Line ID
	 * @return Line ID
	 * @see #lineID */
	public String getLineID() { return lineID; }
	/** Get date of going
	 * @return date of going
	 * @see #date */
	public Date getDate() { return date; }
	/** Get number of adults going
	 * @return number of adults going
	 * @see #noOfAdults */
	public int getNoOfAdults() { return noOfAdults; }
	/** Get number of children going
	 * @return number of children going
	 * @see #noOfChildren */
	public int getNoOfChildren() { return noOfChildren; }
	/** Get number of toddlers going
	 * @return number of toddlers going
	 * @see #noOfToddler */
	public int getNoOfToddler() { return noOfToddler; }
	/** Get tour fee
	 * @return tour fee
	 * @see #tourFee */
	public double getTourFee() { return tourFee; }
	/** Get total fee
	 * @return total fee
	 * @see #totalFee */
	public double getTotalFee() { return totalFee; }
	/** Get special request of customer
	 * @return special request of customer
	 * @see #specialRequest */
	public String getSpecialRequest() { return specialRequest; }
	/** Get index for the list of capacity corresponding to the correct
	 * date of going
	 * @return index for the list of capacity corresponding to the correct
	 * date of going
	 * @see #indexCapacity */
	public int getIndexCapacity() { return indexCapacity; }

	/** Set currently booking tour
	 * @param tour currently booking tour */
	public void setTour(Tour tour) { this.tour = tour; }
	/** Set Line ID
	 * @param lineID Line ID */
	public void setLineID(String lineID) { this.lineID = lineID; }
	/** Set date of going
	 * @param date date of going */
	public void setDate(Date date) { this.date = date;	 }
	/** Set number of adults going
	 * @param noOfAdults number of adults going */
	public void setNoOfAdults(int noOfAdults) { this.noOfAdults = noOfAdults; }
	/** Set number of children going
	 * @param noOfChildren number of children going */
	public void setNoOfChildren(int noOfChildren) { this.noOfChildren = noOfChildren; }
	/** Set number of toddlers going
	 * @param noOfToddler number of toddlers going */
	public void setNoOfToddler(int noOfToddler) { this.noOfToddler = noOfToddler; }
	/** Set the tour fee with the corresponding date of going
	 * @see #tourFee */
	public void setTourFee() {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			tourFee = tour.getWeekendPrice();
		} else {
			tourFee = tour.getWeekdayPrice();
		}
	}
	/** Set total fee for the booking
	 * @param totalFee total fee for the booking */
	public void setTotalFee(double totalFee) { this.totalFee = totalFee; }
	/** Set special request (if any)
	 * @param specialRequest special request (if any)g */
	public void setSpecialRequest(String specialRequest) { this.specialRequest = specialRequest; }
	/** Set index for the list of capacity corresponding to the correct
	 * date of going
	 * @param indexCapacity for the list of capacity corresponding to the correct
	 * date of going
	 * @see #indexCapacity */
	public void setIndexCapacity(int indexCapacity) { this.indexCapacity = indexCapacity; }

	/** This method will reset all class fields as null or -1.*/
	public void clearAllData() {
		this.tour = null;
		this.lineID = null;
		this.date = null;
		this.noOfAdults = -1;
		this.noOfChildren = -1;
		this.noOfToddler = -1;
		this.tourFee = -1.0;
		this.totalFee = -1.0;
		this.specialRequest = null;
	}

}