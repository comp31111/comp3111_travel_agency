package com.example.bot.spring;

import java.util.ArrayList;
import java.util.Calendar;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.ImageMessage;

import lombok.extern.slf4j.Slf4j;

/** A class matching the question asked to an answer 
 * and push the answer to the customer.
 * @see FunctionStrategy*/
@Slf4j
public class SearchFAQ implements FunctionStrategy {

	//FAQ answers
	/** Answer to question about how to apply */
	private static final String HOW_TO_APPLY = "Customers shall approach the company by phone or visit our store (in Clearwater bay) with the choosen tour code and departure date. If it is not full, customers will be advised by the staff to pay the tour fee. Tour fee is non refundable. Customer can pay their fee by ATM to 123-345-432-211 of ABC Bank or by cash in our store. Customer shall send their pay-in slip to us by email or LINE.";
	/** Answer to question about gathering point */
	private static final String GATHERING_POINT = "We gather at the gathering spot \"Exit A, Futian port, Shenzhen\" at 8:00AM on the departure date. We dismiss at the same spot after the tour. \n(See the below picture.)";
	/** Answer to question about cancelled tour */
	private static final String CANCELLED_TOUR = "In case a tour has not enough people or bad weather condition and the tour is forced to cancel, customers will be informed 3 days in advanced. Either change to another tour or refund is avaliable for customers to select. However, due to other reasons such as customers' personal problem no refund can be made.";
	/** Answer to question about additional charge */
	private static final String ADDITIONAL_CHARGE = "Each customer need to pay an additional service charge at the rate $60/day/person, on top of the tour fee. It is collected by the tour guide at the end of the tour.";
	/** Answer to question about transportation */
	private static final String TRANSPORTATION = "A tour bus";
	/** Answer to question about tour guide's contact */
	private static final String TOUR_GUIDE_CONTACT = "Each tour guide has a LINE account and he will add the customers as his friends before the departure date. You can contact him/her accordingly.";
	/** Answer to question about insurance */
	private static final String INSURANCE = "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company.";
	/** Answer to question about hotel's bed */
	private static final String HOTEL_BED = "All rooms are twin beds. Every customer needs to share a room with another customer. If a customer prefer to own a room by himself/herself, additional charge of 30% will be applied.";
	/** Answer to question about visa */
	private static final String VISA = "Please refer the Visa issue to the immigration department of China. The tour are assembled and dismissed in mainland and no cross-border is needed. However, you will need a travelling document when you check in the hotel.";
	/** Answer to question about swimming suit */
	private static final String SWIMMING_SUIT = "You need swimming suit in a water theme park or a hot spring resort. Otherwise you may not use the facility.";
	/** Answer to question about vegetatian */
	private static final String VEGETARIAN = "Sorry, we don't serve vegetarian.";
	/** Answer to question about children tour fee */
	private static final String CHILDREN_TOUR_FEE = "Age below 3 (including 3) is free. Age between 4 to 11 (including 4 and 11) has a discount of 20% off. Otherwise full fee applies. The same service charge is applied to all age customers.";
	/** Answer to question about late */
	private static final String LATE = "You shall contact the tour guide if you know you will be late and see if the tour guide can wait a little bit longer. No refund or make up shall be made if a customer is late for the tour.";
	
	/** Database engine */
	private SQLDatabaseEngine database = new SQLDatabaseEngine();

	/** This method asks customer for personal details, verify the details and
	 * if the details are correct, customer details will be updated in database.
	 * This function is called when customer wants to book tour and he/she has no
	 * record in the database.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer */
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception {

		ArrayList<Message> messages = new ArrayList<Message>();
		String[] faq9 = {"visa", "pass", "passport"};
		// FAQ 9: VISA
		for (int i = 0; i < tokens.length; i++) {
			for (String s: faq9) {
				if (tokens[i].equals(s)) {
					messages.add(new TextMessage(VISA));
					return messages;
				}
			}
		}

		// Check for question words and store them
		String[] questionWord = {"what", "where", "which", "how", "do", "does", "can", "could", "will", "would", "is", "are"};
		boolean[] containQW = {false, false, false, false, false, false, false, false, false, false, false, false};
		boolean question = false;
		for (int i = 0; i < questionWord.length; i++) {
			if (text.contains(questionWord[i])) {
				if (!question)
					question = true;
				containQW[i] = true;
			}
		}

		// FAQ 1: How to apply?
		String faq1 = "apply";
		if (containQW[0] || containQW[3]) {
			if (text.contains(faq1)) {
				messages.add(new TextMessage(HOW_TO_APPLY));
				return messages;
			}
		}

		// FAQ 2: Gathering spot
		String[] faq2 = {"gather", "meet", "assemble", "dismiss", "gathering", "meeting", "dismissing"};
		if (containQW[1] || containQW[10] || containQW[11]) {
			for (int i = 0; i < tokens.length; i++) {
				for (String s: faq2) {
					if (tokens[i].equals(s)) {
						messages.add(new TextMessage(GATHERING_POINT));
						//URL url = this.getClass().getResource("/static/gather.jpg");
						String url = "https://raw.githubusercontent.com/khwang0/2017F-COMP3111/master/Project/topic%201/gather.jpg";
						messages.add(new ImageMessage(url, url));
						return messages;
					}
				}
			}
		}

		// FAQ 3: Cancelled
		String faq3 = "cancel";
		if (text.contains("tour")) {
			if (containQW[0] || containQW[3]) {
				if (text.contains(faq3)) {
					messages.add(new TextMessage(CANCELLED_TOUR));
					return messages;
				}
			}
		}

		// FAQ 4: Additional charge
		String[] faq41 = {"additional", "extra"};
		boolean faq4First = false;
		String[] faq42 = {"charge", "fee", "cost", "charges", "fees", "costs"};
		boolean faq4Second = false;
		String faq4 = "surcharge";
		if (containQW[0] || containQW[3] || containQW[10] || containQW[11]) {
			for (int i = 0; i < tokens.length; i++) {
				if (!faq4First) {
					for (String s1: faq41) {
						if (tokens[i].equals(s1)) {
							faq4First = true;
							continue;
						}
					}
				}
				if (!faq4Second){
					for (String s2: faq42) {
						if (tokens[i].equals(s2)) {
							faq4Second = true;
							continue;
						}
					}
				}
				if (faq4First && faq4Second) {
					break;
				}
			}
			if (faq4First && faq4Second) {
				messages.add(new TextMessage(ADDITIONAL_CHARGE));
				return messages;
			} else {
				if (text.contains(faq4)) {
					messages.add(new TextMessage(ADDITIONAL_CHARGE));
					return messages;
				}
			}
		}

		// FAQ 5: Transportation in Guangdong
		String[] faq5 = {"transportation", "transport", "go", "goes"};
		if (containQW[0] || containQW[3]) {
			if (text.contains("guangdong")) {
				for (int i = 0; i < tokens.length; i++) {
					for (String s: faq5) {
						if (tokens[i].equals(s)) {
							messages.add(new TextMessage(TRANSPORTATION));
							return messages;
						}
					}
				}
			}
		}

		// FAQ 6: Contact tour guide
		String[] faq6 = {"contact", "find", "talk", "reach", "speak"};
		if (text.contains("tour guide")) {
			if (containQW[0] || containQW[3] || containQW[6] || containQW[7]) {
				for (int i = 0; i < tokens.length; i++) {
					for (String s: faq6) {
						if (tokens[i].contains(s)) {
							messages.add(new TextMessage(TOUR_GUIDE_CONTACT));
							return messages;
						}
					}
				}
			}
		}

		// FAQ 7: Insurance
		String faq7 = "insurance";
		if (containQW[0] || containQW[3] || containQW[4] || containQW[5] || containQW[8] || containQW[9] || containQW[10] || containQW[11]) {
			for (String s: tokens) {
				if (s.equals(faq7)) {
					messages.add(new TextMessage(INSURANCE));
					return messages;
				}
			}
		}

		// FAQ 8: Bed arrangement in hotel
		String[] faq81 = {"bed", "room"};
		boolean faq8First = false;
		String[] faq82 = {"arrangement", "arrange", "type", "single", "double", "twin", "number", "hotel"};
		for (String s: faq81) {
			if (text.contains(s)) 
				faq8First = true;
		}
		if (faq8First) {
			for (int i = 0; i < tokens.length; i++) {
				for (String s: faq82) {
					if (tokens[i].equals(s)) {
						messages.add(new TextMessage(HOTEL_BED));
						return messages;
					}
				}
			}
		}

		// FAQ 10: Swimming suit
		String[] faq10 = {"swimming suit", "bathing suit", "beachwear", "bikini", "swimsuit", "swimwear"};
		if (containQW[0] || containQW[3] || containQW[4]|| containQW[5] || containQW[6] || containQW[7] || containQW[8] || containQW[9]) {
			for (String s: faq10) {
				if (text.contains(s)) {
					messages.add(new TextMessage(SWIMMING_SUIT));
					return messages;
				}
			}
		}

		// FAQ 11: Vegeterian
		String faq11 = "vegetarian";
		if (containQW[4] || containQW[5] || containQW[8] || containQW[9]) {
			if (text.contains(faq11)) {
				messages.add(new TextMessage(VEGETARIAN));
				return messages;
			}
		}

		// FAQ 12: Children fee
		String[] faq121 = {"fee", "charge", "price", "cost"};
		boolean faq12First = false;
		String[] faq122 = {"kid", "child", "toodler", "kids", "toodlers", "children"};
		boolean faq12Second = false;
		String faq12 = "how much";
		if (containQW[0] || containQW[3] || containQW[4] || containQW[5] || containQW[8] || containQW[9] || containQW[10] || containQW[11]) {
			if (text.contains(faq12)) {
				for (int i = 0; i < tokens.length; i++) {
					for (String s: faq122) {
						if (tokens[i].equals(s)) {
							messages.add(new TextMessage(CHILDREN_TOUR_FEE));
							return messages;
						}
					}
				}
			} else {
				for (int i = 0; i < tokens.length; i++) {

					if (!faq12First) {
						for (String s: faq121) {
							if (tokens[i].equals(s)) {
								faq12First = true;
								break;
							}
						}
					}
					if (!faq12Second) {
						for (String s: faq122) {
							if (tokens[i].equals(s)) {
								faq12Second = true;
								break;
							}
						}
					}
					if (faq12First && faq12Second) {
						break;
					}
				}
			}
			if (faq12First && faq12Second) {
				messages.add(new TextMessage(CHILDREN_TOUR_FEE));
				return messages;
			}
		}

		// FAQ 13: Late in departure date
		String[] faq13 = {"leave", "depart", "departure", "leaving"};
		if (containQW[0] || containQW[4] || containQW[5] || containQW[6] || containQW[7] || containQW[8] || containQW[9]) {
			if (text.contains("late")) {
				for (int i = 0; i < tokens.length; i++) {
					for (String s: faq13) {
						if (tokens[i].equals(s)) {
							messages.add(new TextMessage(LATE));
							return messages;
						}
					}
				}
			}
		}

		database.insertCustomerServiceQuestions(customer.getCustomerID(), new java.sql.Date(Calendar.getInstance().getTime().getTime()), text, false);
		customer.setStatus(-2);
		messages.add(new TextMessage("Sorry we currently do not have this answer."));
		return messages;
	}

}