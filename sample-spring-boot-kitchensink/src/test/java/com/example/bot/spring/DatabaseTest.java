package com.example.bot.spring;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = { DatabaseTest.class, SQLDatabaseEngine.class })
public class DatabaseTest {
	@Autowired
	private SQLDatabaseEngine db;


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void createCustomer1() throws Exception {
		boolean result = false;
		try {
			result = this.db.createCustomer("Uc172a4eb8f5b2aeda65bd210a51edbac", "Alfred", "11111111", 20);
		} catch (Exception e) {
		}
		assertTrue(result);
	}


	/**
	 * Typical value
	 * Invalid input - length of phone is larger than 8
	 *
	 * @throws Exception
	 */

	@Test
	public void createCustomer2() throws Exception {
		boolean result = true;
		try {
			result = this.db.createCustomer("U53bf0b36e6e4af47a6f51c23392e5f90", "Alex", "123456789", 18);
		} catch (Exception e) {
		}
		assertFalse(result);
	}

	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void createCustomer3() throws Exception {
		boolean result = false;
		try {
			result = this.db.createCustomer("U53bf0b36e6e4af47a6f51c23392e5f93", "", "", -1);
		} catch (Exception e) {
		}
		assertTrue(result);
	}

	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void searchBookingRecord() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.searchBookingRecord("U53bf0b36e6e4af47a6f51c23392e5f88");
		} catch (Exception e) {
		}
		assertTrue(result!=null);
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void getUpcomingTour() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.getUpcomingTour();
		} catch (Exception e) {
		}
		assertTrue(result!=null);
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void getOutstandingFee() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.getOutstandingFee();
		} catch (Exception e) {
		}
		assertTrue(result!=null);
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void getLatestSavedTour() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.getLatestSavedTour();
		} catch (Exception e) {
		}
		assertTrue(result!=null);
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void searchTour1() throws Exception {
		ArrayList<Tour> result = null;
		// String[] keyword = {adj, time, place, chosenHotel, numOfDays, departureDate, price, tid};
		String[] temp = {"Hot spring", "", "", "", "", "", "", ""};
		try {
			result = this.db.searchTour(temp);
		} catch (Exception e) {
		}
		assertTrue(result!=null); // assert with real Tour information
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void searchTour2() throws Exception {
		ArrayList<Tour> result = null;
		// String[] keyword = {adj, time, place, chosenHotel, numOfDays, departureDate, price, tid};
		String[] temp = {"", "", "", "", "2", "", "", ""};
		try {
			result = this.db.searchTour(temp);
		} catch (Exception e) {
		}
		assertTrue(result!=null); // assert with real Tour information
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void searchTour3() throws Exception {
		ArrayList<Tour> result = null;
		// String[] keyword = {adj, time, place, chosenHotel, numOfDays, departureDate, price, tid};
		String[] temp = {"", "", "", "Yihua", "", "", "", ""};
		try {
			result = this.db.searchTour(temp);
		} catch (Exception e) {
		}
		assertTrue(result!=null); // assert with real Tour information
	}


	/**
	 * Other value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void searchTour4() throws Exception {
		ArrayList<Tour> result = null;
		// String[] keyword = {adj, time, place, chosenHotel, numOfDays, departureDate, price, tid};
		String[] temp = {"", "", "", "", "a", "", "", ""};
		try {
			result = this.db.searchTour(temp);
		} catch (Exception e) {
		}
		assertTrue(result!=null); // assert with real Tour information
	}


	/**
	 * Typical value
	 * Valid input - User has existing booking record(s)
	 *
	 * @throws Exception
	 */

	@Test
	public void recommendTour1() throws Exception {
		ArrayList<String> result = null;
		try {
			result = this.db.recommendTour("U9f8415186be8d7381ba6f45444634f20");
		} catch (Exception e) {
		}
		assertTrue(result!=null); // assert with real Tour information
	}


	/**
	 * Typical value
	 * Valid input - User does not have existing booking record(s)
	 *
	 * @throws Exception
	 */

	@Test
	public void recommendTour2() throws Exception {
		ArrayList<String> result = null;
		try {
			result = this.db.recommendTour("U9f8415186be8d7381ba6f45444123456");
		} catch (Exception e) {
		}
		assertTrue(result==null); // assert with real Tour information
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void bookTour1() throws Exception {
		boolean result = false;
		Date temp_date = Date.valueOf("2017-12-14");
		try {
			result = this.db.bookTour("U53bf0b36e6e4af47a6f51c23392e5f88", "2D004", temp_date, 1, 0, 0, 0, 399, "null", false);
		} catch (Exception e) {
		}
		assertTrue(result);
	}


	/**
	 * Typical input
	 * Invalid input - length of request larger than 50
	 *
	 * @throws Exception
	 */

	@Test
	public void bookTour2() throws Exception {
		boolean result = true;
		Date temp_date = Date.valueOf("2017-12-14");
		try {
			result = this.db.bookTour("U53bf0b36e6e4af47a6f51c23392e5f88", "2D004", temp_date, 1, 0, 0, 0, 399, "abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde", false);
		} catch (Exception e) {
		}
		assertFalse(result);
	}

	@Test
	public void findNewTour() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.findNewTour();
		} catch (Exception e) {
		}
		assertTrue(result != null);
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void checkExistingCustomer1() throws Exception {
		boolean result = false;
		Customer dummy_customer = new Customer();
		try {
			result = this.db.checkExistingCustomer("U53bf0b36e6e4af47a6f51c23392e5f88", dummy_customer);
		} catch (Exception e) {
		}
		assertTrue(result);
	}


	/**
	 * Other value
	 * Invalid input
	 *
	 * @throws Exception
	 */

	@Test
	public void checkExistingCustomer2() throws Exception {
		boolean result = true;
		Customer dummy_customer = new Customer();
		try {
			result = this.db.checkExistingCustomer("123", dummy_customer);
		} catch (Exception e) {
		}
		assertFalse(result);
	}

	@Test
	public void getExistingLineID() throws Exception {
		ArrayList<String> result = null;
		try {
			result = this.db.getExistingLineID();
		} catch (Exception e) {
		}
		assertTrue(result != null);
	}


	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void insertCustomerServiceQuestions1() throws Exception {
		boolean result = false;
		Date temp_date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		Random rand = new Random();
		int n = rand.nextInt(1000) + 1;
		try {
			result = this.db.insertCustomerServiceQuestions("A111222", temp_date, "This is question " + n, false);
		} catch (Exception e) {
		}
		assertTrue(result);
	}

	/**
	 * Other value
	 * Invalid input
	 *
	 * @throws Exception
	 */

	@Test
	public void insertCustomerServiceQuestions2() throws Exception {
		boolean result = true;
		Date temp_date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		try {
			result = this.db.insertCustomerServiceQuestions("A111222", temp_date, "abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdea", false);
		} catch (Exception e) {
		}
		assertFalse(result);
	}

	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void updateDiscountedCustomer() throws Exception {
		boolean result = false;
		Date temp_date = Date.valueOf("2017-10-17");
		try {
			result = this.db.updateDiscountedCustomer("3D991", temp_date);
		} catch (Exception e) {
		}
		assertTrue(result);
	}

	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void resetDiscountedTour() throws Exception {
		boolean result = false;
		Date temp_date = Date.valueOf("2017-10-17");
		try {
			result = this.db.resetDiscountedTour("3D991", temp_date);
		} catch (Exception e) {
		}
		assertTrue(result);
	}

	/**
	 * Typical value
	 * Valid input
	 *
	 * @throws Exception
	 */

	@Test
	public void getSavedTour1() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.getSavedTour("A111222");
		} catch (Exception e) {
		}
		assertTrue(result != null);
	}

	/**
	 * Other value
	 * Invalid input
	 *
	 * @throws Exception
	 */

	@Test
	public void getSavedTour2() throws Exception {
		ArrayList<String[]> result = null;
		try {
			result = this.db.getSavedTour("123");
		} catch (Exception e) {
		}
		assertFalse(result.size() > 0);
	}

	@Test
	public void checkDiscountExceedLimit1() throws Exception {
		boolean result = false;
		Date temp_date = Date.valueOf("2017-11-25");
		try {
			result = this.db.checkDiscountExceedLimit("A222449", "3D075", temp_date, 1);
		} catch (Exception e) {
		}
		assertTrue(result);
	}

	@Test
	public void checkDiscountExceedLimit2() throws Exception {
		boolean result = true;
		Date temp_date = Date.valueOf("2017-11-08");
		try {
			result = this.db.checkDiscountExceedLimit("A111222", "2D001", temp_date, 1);
		} catch (Exception e) {
		}
		assertFalse(result);
	}

	@Test
	public void checkDiscountExceedLimit3() throws Exception {
		boolean result = true;
		Date temp_date = Date.valueOf("2017-11-25");
		try {
			result = this.db.checkDiscountExceedLimit("A222449", "3D075", temp_date, 0);
		} catch (Exception e) {
		}
		assertFalse(result);
	}

	@Test
	public void checkTourDiscount() throws Exception {
		DiscountedTour result = null;
		try {
			result = this.db.checkTourDiscount();
		} catch (Exception e) {
		}
		assertTrue(result != null);
	}

	@Test
	public void addSavedTour() throws Exception {
		boolean result = true;
		try {
			result = this.db.addSavedTour("A111222", "2D001");
		} catch (Exception e) {
		}
		assertFalse(result);
	}
}
