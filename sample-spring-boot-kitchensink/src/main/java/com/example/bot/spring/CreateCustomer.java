package com.example.bot.spring;

import java.util.ArrayList;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import lombok.extern.slf4j.Slf4j;
/**
 * A class to ask for customer details to create customer for booking tours
 * @see FunctionStrategy
 * */
@Slf4j
public class CreateCustomer implements FunctionStrategy {
	/**Database Engine*/
	private SQLDatabaseEngine database = new SQLDatabaseEngine();

	/** This method asks customer for personal details, verify the details and
	 * if the details are correct, customer details will be updated in database.
	 * This function is called when customer wants to book tour and he/she has no
	 * record in the database.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer*/
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception{

		ArrayList<Message> messages = new ArrayList<Message>();
		boolean success = false;		

		if (customer.getName() == null && (text.length() > 0 && text.length() <= 30)) {
			customer.setName(text);
			TextMessage message = new TextMessage("What is your phone number?");
			messages.add(message);
			return messages;
		}
		else if (customer.getName() == null && !(text.length() > 0 && text.length() <= 30)) {
			TextMessage message = new TextMessage("Name is too long. Please re-enter.");
			messages.add(message);
			return messages;
		}
		else if(customer.getPhoneNo() == null && (text.length() == 8)) {
			customer.setPhoneNo(text);
			TextMessage message = new TextMessage("What is your age?");
			messages.add(message);
			return messages;
		}
		else if (customer.getPhoneNo() == null && (text.length() != 8)) {
			TextMessage message = new TextMessage("Invalid phone number. Please re-enter.");
			messages.add(message);
			return messages;
		}
		else if (customer.getAge() == -1 && (Integer.parseInt(text) > 0 && Integer.parseInt(text) <= 100)) {
			customer.setAge(Integer.parseInt(text));
			try {
				success = database.createCustomer(customer.getLineID(), customer.getName(), customer.getPhoneNo(), customer.getAge());
				success = database.checkExistingCustomer(customer.getLineID(), customer);
			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
		else if (customer.getAge() == -1 && !(Integer.parseInt(text) > 0 && Integer.parseInt(text) <= 100)) {
			TextMessage message = new TextMessage("Invalid age. Please re-enter.");
			messages.add(message);
			return messages;
		}

		if(success) {
			customer.setStatus(3);
			TextMessage message1 = new TextMessage("You have successfully create your account.");
			TextMessage message2 = null;
			if (customer.getBooking().getTour() instanceof DiscountedTour) {
				message2 = new TextMessage("How many adults?");
			} else {
				message2 = new TextMessage("Which date you are going?\n(Please enter in D/M format)");
			}
			messages.add(message1);
			messages.add(message2);
			return messages;
		}
		else {
			customer.setStatus(-2);
			customer.setAge(-1);
			customer.setPhoneNo(null);
			customer.setName(null);
			InProgressBooking booking = customer.getBooking();
			booking.clearAllData();
			customer.setBooking(booking);
			TextMessage message1 = new TextMessage("You fail to create the account. Please contact our customer service for help.");
			TextMessage message2 = new TextMessage("What other help do you need?");
			messages.add(message1);
			messages.add(message2);
			return messages;
		}
	}
}