/*
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.example.bot.spring;

import java.io.StringWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.text.DecimalFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.io.ByteStreams;

import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.MessageContentResponse;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.event.BeaconEvent;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.JoinEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.UnfollowEvent;
import com.linecorp.bot.model.event.message.AudioMessageContent;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.RoomSource;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.AudioMessage;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.ImagemapMessage;
import com.linecorp.bot.model.message.LocationMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.StickerMessage;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.imagemap.ImagemapArea;
import com.linecorp.bot.model.message.imagemap.ImagemapBaseSize;
import com.linecorp.bot.model.message.imagemap.MessageImagemapAction;
import com.linecorp.bot.model.message.imagemap.URIImagemapAction;
import com.linecorp.bot.model.message.template.ButtonsTemplate;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import com.linecorp.bot.model.message.template.ConfirmTemplate;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

/** 
 * A class handling events from customer
 * */
@Slf4j
@LineMessageHandler
public class KitchenSinkController {
	
	/** 
	 * The array list storing customers
	 * */
	private static ArrayList<Customer> customerList = new ArrayList<Customer>();
	/** 
	 * The current customer in controller
	 * */
	private Customer customer;

	/**
	 * The Line Messaging Client to push messages
	 * 
	 * @see #reply(String, List)
	 */
	@Autowired
	private LineMessagingClient lineMessagingClient;
	
	/** This method handles text message event when customer input text to the chatbot
	 * 
	 * @param event the message event received.
	 * */
	@EventMapping
	public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) throws Exception {
		log.info("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		log.info("This is your entry point:");
		log.info("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		TextMessageContent message = event.getMessage();
		handleTextContent(event.getReplyToken(), event, message);
	}

	/** This method handles unfollows event when customer block or remove chatbot as friend
	 * 
	 * @param event the message event received.
	 * */
	@EventMapping
	public void handleUnfollowEvent(UnfollowEvent event) {
		log.info("unfollowed this bot: {}", event);
	}
	
	/** This method handles follow event when customer add the chatbot as friend. It stores
	 * the lineID of the customer into the database.
	 * 
	 * @param event the message event received.
	 * */
	@EventMapping
	public void handleFollowEvent(FollowEvent event) {
		log.info("get into follow event");
		try {
			String lineID = event.getSource().getUserId();

			if (!checkExistingCustomerInController(lineID, false)) {
				createCustomerInController(lineID, false);
			}
			
			if (!database.checkExistingCustomer(lineID, customer)) {
				if (database.createCustomer(lineID, "", "", -1)) {
					log.info("Successfully inserted customer");
				} else {
					log.info("cannot insert customer into db");
				}
			}
		} catch (Exception e) {
			log.info("cannot insert customer into db");
			log.info(e.getMessage());
		}
		String replyToken = event.getReplyToken();
		this.replyText(replyToken, "Welcome! Thank you for adding our chatbot as friend!");
	}
	
	/** This method handles join event when customer adds chatbot to a group
	 * 
	 * @param event the message event received.
	 * */
	@EventMapping
	public void handleJoinEvent(JoinEvent event) {
		String replyToken = event.getReplyToken();
		this.replyText(replyToken, "Joined " + event.getSource());
	}
	
	/** This method handles post back event when customer click into the template message that will send
	 * data using POST. It is called when the customer click on the button message for discounted campaign.
	 * It will set customer into booking status (3) and set the booking of the customer with the postback
	 * data.
	 * 
	 * @param event the message event received.
	 * */
	@EventMapping
	public void handlePostbackEvent(PostbackEvent event) {
		String[] data = event.getPostbackContent().getData().split(":");
		String name = data[0];
		String tourID = data[1];
		String description = data[2];
		double weekendPrice = Double.parseDouble(data[3]);
		double weekdayPrice = Double.parseDouble(data[4]);
		int days = Integer.parseInt(data[5]);
		int capacity = Integer.parseInt(data[6]);
		String lineID = event.getSource().getUserId();
		
		// Discounted Tour promotion
		if (data.length == 8) {
			try {
				// Set customer in controller
				if (!checkExistingCustomerInController(lineID, true)) {
					createCustomerInController(lineID, true);
				}

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dateFormat.parse(data[7]);
				//String lineID = event.getSource().getUserId();
				Tour t = new DiscountedTour(name, tourID, description, weekendPrice, weekdayPrice, days, capacity, date);
				InProgressBooking booking = customer.getBooking();
				booking.setTour(t);
				booking.setLineID(lineID);
				booking.setDate(date);
				customer.setBooking(booking);

				// Check if customer exist in database
				if (!database.checkExistingCustomer(lineID, customer)) {
					customer.setStatus(5);
					ArrayList<Message> messages = new ArrayList<Message>();
					TextMessage message1 = new TextMessage("You have not booked any tours before. Please enter your personal information.");
					TextMessage message2 = new TextMessage("What is your name?");
					messages.add(message1);
					messages.add(message2);
					this.reply(event.getReplyToken(), messages);
				} else {
					this.replyText(event.getReplyToken(), "How many adults?");
				}
			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
	}
	
	/** This method overrides the reply function and convert message into a list, and call the 
	 * reply function to send. 
	 * 
	 * @param replyToken this is where the message should be replied to. It cannot be null.
	 * @param message the message to be sent. It cannot be null.
	 * @see #reply(String, List)
	 * */
	private void reply(@NonNull String replyToken, @NonNull Message message) {
		reply(replyToken, Collections.singletonList(message));
	}

	/** This method send the messages to the specific replyToken. 
	 * 
	 * @param replyToken this is where the message should be replied to. It cannot be null.
	 * @param message the message to be sent. It cannot be null.
	 * @see LineMessagingClient#replyMessage(ReplyMessage)
	 * */
	private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
		try {
			BotApiResponse apiResponse = lineMessagingClient.replyMessage(new ReplyMessage(replyToken, messages)).get();
			log.info("Sent messages: {}", apiResponse);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	/** This method convert string into a message, and call the reply function to send. 
	 * 
	 * @param replyToken this is where the message should be replied to. It cannot be null.
	 * @param message the message to be sent. It cannot be null.
	 * @see #reply(String, Message)
	 * */
	private void replyText(@NonNull String replyToken, @NonNull String message) {
		if (replyToken.isEmpty()) {
			throw new IllegalArgumentException("replyToken must not be empty");
		}
		if (message.length() > 1000) {
			message = message.substring(0, 1000 - 2) + "..";
		}
		this.reply(replyToken, new TextMessage(message));
	}

	/** This method convert sticker content into a sticker message, and call the reply function to send. 
	 * 
	 * @param replyToken this is where the message should be replied to.
	 * @param content the sticker message content to be converted to sticker message
	 * @see #reply(String, Message)
	 * */
	private void handleSticker(String replyToken, StickerMessageContent content) {
		reply(replyToken, new StickerMessage(content.getPackageId(), content.getStickerId()));
	}
	
	/** This method handles the text content of customer input and identifies the corresponding action.
	 * 
	 * @param replyToken this is where the message should be replied to. It cannot be null.
	 * @param event the event triggered
	 * @param content the text content of customer input
	 * 
	 * */
	private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws Exception {
		//log.info("status: {}", customer.getStatus());
		log.info("enter handle text");
		
		//Get user input
		String text = content.getText().toLowerCase();

		//Get and set the Line ID of the user
		String lineID = event.getSource().getUserId();

		//check if customer exists in the online session
		//create new customer if not exist in the online session
		if (!checkExistingCustomerInController(lineID, false)) {
			createCustomerInController(lineID, false);
		}
		
		//Get customer's data if the user exists in the database
		if (customer.getCustomerID() == null) {
			database.checkExistingCustomer(lineID, customer);
		}

		//Load Tokenizer model
		InputStream tokenis = this.getClass().getResourceAsStream("/static/en-token.bin");
		TokenizerModel modelToken = new TokenizerModel(tokenis);

        //Load POS tagging model
		InputStream taggeris = this.getClass().getResourceAsStream("/static/en-pos-maxent.bin");
		POSModel modelPOS = new POSModel(taggeris);
		POSTaggerME tagger = new POSTaggerME(modelPOS);

        //Tokenize the sentence
		Tokenizer tokenizer = new TokenizerME(modelToken);
		String[] tokens = tokenizer.tokenize(text);

        //Generate tags[]
		String tags[] = tagger.tag(tokens);

		//convert the input message to lower case
		for(int i = 0; i < tokens.length; i++) {
			tokens[i] = tokens[i].toLowerCase();
			log.info("show tag: {}", tags[i]);/////////////////////////////////////////////////////////////////////////////////////////////////debug used
		}
		//log.info("status: {}", customer.getStatus());
		

		//identify keyword
		if (customer.getStatus() < 1) {
			keywordClassification(text.toLowerCase(), tokens, tags);
		}

		//error message
		if (customer.getStatus() == -2) {
			log.info("error message in controller");
			this.replyText(replyToken, "Sorry I don't understand what you are saying.");
		}

		//no respond needed
		if (customer.getStatus() == -3) {
			return;
		}

		FunctionFactory functionFactory = new FunctionFactory();
		FunctionStrategy functionStrategy = functionFactory.getFunction(customer.getStatus());
		ArrayList<Message> message = functionStrategy.functionOperation(text, tokens, tags, customer);
		this.reply(replyToken, message);
	}

	/**
	 * This method is used to identify some keywords from the user input, then
	 * set the status, which is used to direct the user to the specific function.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	**/
	private void keywordClassification(String text, String[] tokens, String[] tags){

		String[] greeting = {"hi", "hello", "hey", "yo", "is anyone there"};
		String[] exit = {"bye", "byebye", "goodbye", "thanks", "see you",  "thank you"};
		String[] questionWord = {"what", "when", "where", "which", "how", "do", "does", "did", "can", "could", "will", "would", "is", "are", "was", "were"};
		String[] questionVerb = {"ask", "know", "join", "book", "search", "find", "choose", "go", "travel", "have"};
		String[] noRespond = {"yes", "ok"};
		String[] faqSpecialCase = {"visa", "pass", "passport"};
		//private static final String[] tempVB = {"help"};

		
		//remove the data in progress booking record
		InProgressBooking booking = customer.getBooking();
		booking.clearAllData();
		customer.setBooking(booking);
		
		//remove the data of the temporary TourArr
		ArrayList<Tour> tourArrTemp = customer.getTourArr();
		for(int i = 0; i < tourArrTemp.size(); i++) {
			tourArrTemp.remove(i);
		}
		customer.setTourArr(tourArrTemp);

		//check whether it is a greeting
		for(int i = 0; i < greeting.length - 1; i++) {
			if (greeting[i].equals(tokens[0]) || text.contains(greeting[4])) {
				customer.setStatus(0); 
				return;
			}
		}

		//check whether the user wants to end the conversation
		for(int i = 0; i < exit.length; i++) {
			if (tokens.length > 1) {
				for(int j = 0; j < tokens.length - 1; j++) {
					if ((tokens[j]+" "+tokens[j+1]).contains(exit[i])) {
						customer.setStatus(-1);
						return;
					}
				}
			}
			else if (exit[i].equals(tokens[0]) || tokens[0].equals("no")) {
				customer.setStatus(-1);
				return;
			}
		}

		//no response needed
		for(int i = 0; i < noRespond.length; i++) {
			if(noRespond[i].equals(tokens[0])) {
				customer.setStatus(-3);
				return;
			}
		}

		//search booking record
		for(int i = 0; i < tokens.length; i++) {
			if ((tokens[i].equals("booking") || tokens[i].equals("previous")) && (tokens[i+1].contains("record") || (tokens[i+1].contains("histor")))) {
				customer.setStatus(4);
				return;
			}
		}

		//search saved tours
		for(int i = 0; i < tokens.length; i++) {
			if (tokens[i].equals("saved") && tokens[i+1].contains("tour")) {
				customer.setStatus(6);
				return;
			}
		}

		//check whether the user wants to search tour / book tour
		for(int i = 0; i < tokens.length; i++) {
			for(int j = 0; j < questionVerb.length; j++) {
				if (tokens[i].equals(questionVerb[j])) {
					customer.setStatus(2);
					return;
				}
			}
		}

		//check whether the user wants to search FAQ
		for(int i = 0; i < questionWord.length; i++) {
			if (tokens[0].equals(questionWord[i])) {
				customer.setStatus(1);
				return;
			}
		}

		for(int i = 0; i < faqSpecialCase.length; i++) {
			if (tokens[0].equals(faqSpecialCase[i])) {
				customer.setStatus(1);
				return;
			}
		}
		
		customer.setStatus(-2);
		return;
	}

	/**
	 * This method is used to check whether a customer is in the online session. If yes, and 
	 * there is a promotion and customer chooses to book, direct the customer to booking progress.
	 * @param lineID the line ID of customer
	 * @param promotion true when used in booking discounted tour, otherwise, false
	 * @return if customer is initialized here
	**/
	private boolean checkExistingCustomerInController(String lineID, boolean promotion) {
		if (customerList.size() != 0) {
			for(int i = 0; i < customerList.size(); i++) {
				if (lineID.equals(customerList.get(i).getLineID())) {
					customer = customerList.get(i);
					if (promotion) {
						customer.setStatus(3);
					}
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * This method is used to create a customer in the online session. If there is
	 * a promotion and customer chooses to book, direct the customer to booking progress.
	 * @param lineID the line ID of customer
	 * @param promotion true when used in booking discounted tour, otherwise, false
	**/
	private void createCustomerInController(String lineID, boolean promotion) {
		customer = new Customer();
		if (promotion) {
			customer.setStatus(3);
		}
		customer.setLineID(lineID);
		customerList.add(customer);
	}
	/**
	 * This method is used to create URI.
	 * @param path path to be converted into URI
	 * @return the URI
	**/
	static String createUri(String path) {
		return ServletUriComponentsBuilder.fromCurrentContextPath().path(path).build().toUriString();
	}
	/**
	 * This method initialize the system.
	 * @param args 
	 * */
	private void system(String... args) {
		ProcessBuilder processBuilder = new ProcessBuilder(args);
		try {
			Process start = processBuilder.start();
			int i = start.waitFor();
			log.info("result: {} =>  {}", Arrays.toString(args), i);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		} catch (InterruptedException e) {
			log.info("Interrupted", e);
			Thread.currentThread().interrupt();
		}
	}
	/**
	 * This method save content into the created temporary file.
	 * @param ext 
	 * @param responseBody the content type
	 * */
	private static DownloadedContent saveContent(String ext, MessageContentResponse responseBody) {
		log.info("Got content-type: {}", responseBody);

		DownloadedContent tempFile = createTempFile(ext);
		try (OutputStream outputStream = Files.newOutputStream(tempFile.path)) {
			ByteStreams.copy(responseBody.getStream(), outputStream);
			log.info("Saved {}: {}", ext, tempFile);
			return tempFile;
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	/**
	 * This method creates temporary file for the downloaded content.
	 * @param ext 
	 * @return the temporary file created
	 * @see #saveContent(String, MessageContentResponse)
	 * */
	private static DownloadedContent createTempFile(String ext) {
		String fileName = LocalDateTime.now().toString() + '-' + UUID.randomUUID().toString() + '.' + ext;
		Path tempFile = KitchenSinkApplication.downloadedContentDir.resolve(fileName);
		tempFile.toFile().deleteOnExit();
		return new DownloadedContent(tempFile, createUri("/downloaded/" + tempFile.getFileName()));
	}
	/**
	 * This is the constructor. It initialize the database engine.
	**/
	public KitchenSinkController() {
		database = new SQLDatabaseEngine();
//		itscLOGIN = System.getenv("ITSC_LOGIN");
	}
	/**
	 * This is the database engine.
	**/
	private SQLDatabaseEngine database;
//	private String itscLOGIN;


	/**
	 * The annontation '@Value' is from the package lombok.Value
	 * Basically what it does is to generate constructor and getter for the class below
	 * see https://projectlombok.org/features/Value
	*/
	@Value
	public static class DownloadedContent {
		Path path;
		String uri;
	}


	/**
	 * an inner class that gets the user profile and status message
	 */
	class ProfileGetter implements BiConsumer<UserProfileResponse, Throwable> {
		/**
		 * The controller handling event
		 * */
		private KitchenSinkController ksc;
		/**
		 * The specific token for replying customer
		 * */
		private String replyToken;

		/**
		 * This is the constructor. 
		 * @param ksc the controller handling event
		 * @param replyToken the specific token for replying customer
		 * @see #ksc
		 * @see #replyToken
		 * */
		public ProfileGetter(KitchenSinkController ksc, String replyToken) {
			this.ksc = ksc;
			this.replyToken = replyToken;
		}
		
		/**
		 * This method reply customer to display their name and status.
		 * 
		 * @param profile the user profile
		 * @param throwable error
		 * */
		@Override
		public void accept(UserProfileResponse profile, Throwable throwable) {
			if (throwable != null) {
				ksc.replyText(replyToken, throwable.getMessage());
				return;
			}
			ksc.reply(
				replyToken,
				Arrays.asList(new TextMessage(
					"Display name: " + profile.getDisplayName()),
				new TextMessage("Status message: "
					+ profile.getStatusMessage()))
				);
		}
	}

}