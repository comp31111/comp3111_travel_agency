package com.example.bot.spring;

import lombok.extern.slf4j.Slf4j;

/** A class to determine what operation to call to reply customers.*/
@Slf4j
public class FunctionFactory {

	/** Greeting Operation */
	private final FunctionStrategy greeting = new Greeting();
	/** Exit Operation */
	private final FunctionStrategy exit = new Exit();
	/** Searching FAQ Operation */
	private final FunctionStrategy searchFAQ = new SearchFAQ();
	/** Searching Tour Operation */
	private final FunctionStrategy searchTour = new SearchTour();
	/** Searching Booking Record Operation */
	private final FunctionStrategy searchBookingRecord = new SearchBookingRecord();
	/** Searching Saved Tour Operation */
	private final FunctionStrategy searchSavedTour = new SearchSavedTour();
	/** Booking Tour Operation */
	private final FunctionStrategy booking = new Booking();
	/** Creating Customer in database Operation */
	private final FunctionStrategy createCustomer = new CreateCustomer(); 

	/** This method determines what operations should be called based on the status.
	 * @param status Customer status, indicating what he/she is now doing.
	 * @return operations should be done acoording to the status*/
	public FunctionStrategy getFunction(int status) {

		switch (status) {
			case 0: return greeting;				//Greeting
			case 1: return searchFAQ;  				//Search FAQ
			case 2: return searchTour;  			//Search tour
			case 3: return booking;					//Book tour
			case 4: return searchBookingRecord;		//Search previous record
			case 5: return createCustomer;			//Create customer
			case 6: return searchSavedTour;			//Search saved tour
			case -1: return exit;					//Exit
			default: return null;
		}
	}

}