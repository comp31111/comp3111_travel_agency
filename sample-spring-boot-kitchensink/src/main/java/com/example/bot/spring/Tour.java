package com.example.bot.spring;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
/** A class to hold details of tour */
@Slf4j
public class Tour {
	/** tour name */
    private String name;
	/** tour ID */
    private String tourID;
	/** tour description */
    private String description;
	/** price for weekday */
    private double weekdayPrice;
	/** price for weekend */
    private double weekendPrice;
	/** List of available dates */
    private ArrayList<Date> availableDate;	//format: yyyyMMdd
	/** List of confirmed dates */
    private ArrayList<Date> confirmedDate;	//format: yyyyMMdd
	/** duration of tour */
    private int days;
	/** list of capacity corresponds to different dates */
    private ArrayList<Integer> capacity;

	/** Constructor. It sets all the class fields to either the input parameters
	 * or initialized the array list.
	 * @param name tour name
	 * @param tourID tour ID
	 * @param description tour description
	 * @param weekendPrice the price of the tour in weekend
	 * @param weekdayPrice the price of the tour in weekday
	 * @param days duration of tour*/
    public Tour(String name, String tourID, String description, double weekendPrice, double weekdayPrice, int days) {
        this.name = name;
        this.tourID = tourID;
        this.description = description;
        this.weekdayPrice = weekdayPrice;
        this.weekendPrice = weekendPrice;
        this.availableDate = new ArrayList<Date>();
        this.confirmedDate = new ArrayList<Date>();
        this.days = days;
        this.capacity = new ArrayList<Integer>();
    }

	/** Get tour name
	 * @return tour name
	 * @see #name */
    public String getName() { return name; }
	/** Get tour ID
	 * @return tour ID
	 * @see #tourID */
    public String getTourID() { return tourID; }
	/** Get tour description
	 * @return tour description
	 * @see #description */
    public String getDescription() { return description; }
	/** Get price for weekday
	 * @return price for weekday
	 * @see #weekdayPrice */
    public double getWeekdayPrice() { return weekdayPrice; } 
	/** Get price for weekend
	 * @return price for weekend
	 * @see #weekendPrice */
    public double getWeekendPrice() { return weekendPrice; }
	/** Get list of available dates
	 * @return list of available dates
	 * @see #availableDate */
    public ArrayList<Date> getAvailableDate(){ return availableDate; }
	/** Get list of confirmed dates
	 * @return list of confirmed dates
	 * @see #confirmedDate */
    public ArrayList<Date> getConfirmedDate() { return confirmedDate; }
	/** Get duration of tour
	 * @return duration of tour
	 * @see #days */
    public int getDays() { return days; }
	/** Get list of capacity corresponds to different dates 
	 * @return list of capacity corresponds to different dates 
	 * @see #capacity */
    public ArrayList<Integer> getCapacity() { return capacity; }

	/** Set name
	 * @param name Name */
    public void setName(String name) { this.name = name; }
	/** Set tour ID
	 * @param tourID tour ID */
    public void setTourID(String tourID) { this.tourID = tourID; }
	/** Set tour description
	 * @param description tour description */
    public void setDescription(String description) { this.description = description; }
	/** Set price for weekday
	 * @param price price for weekday */
    public void setWeekdayPrice(double price) { weekdayPrice = price; }
	/** Set price for weekend
	 * @param price price for weekend */
    public void setWeekendPrice(double price) { weekendPrice = price; }
	/** Add a date to the list of available dates
	 * @param date available date */
    public void setAvailableDate(Date date) { availableDate.add(date); }
    //public void setConfirmedDate(Date date) { confirmedDate.add(date); }
    public void setDays(int days) { this.days = days; }
	/** Add a capacity corresponding to dates to the list of available dates
	 * @param availCapacity capacity corresponding to different dates */
    public void setCapacity(int availCapacity) { capacity.add(availCapacity); }
    
}