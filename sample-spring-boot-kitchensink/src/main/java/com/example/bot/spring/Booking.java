package com.example.bot.spring;

import java.util.ArrayList;
import java.util.Date; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

import lombok.extern.slf4j.Slf4j;
/**
 * A class handling booking tour operation
 * @see FunctionStrategy
 * */
@Slf4j
public class Booking implements FunctionStrategy {
	/**Sentence to display after successfully booked the tour*/
	private static final String CONFIRMED_BOOKING_PAYMENT = "Please pay the tour fee by ATM to 123-345-432-211 of ABC Bank or by cash in our store. When you complete the ATM payment, please send the bank in slip to us. Our staff will validate it.";
	/**Database Engine*/
	private SQLDatabaseEngine database = new SQLDatabaseEngine();
	/**Details of the booking*/
	private InProgressBooking booking;
	/**Tour being booked now*/
	private Tour tour;
	/**Line ID of the customer who is booking tour*/
	private String lineID;
	/**Date of going*/
	private Date chosenDate;
	/**Number of adults going*/
	private int adult;
	/**Number of children going*/
	private int children;
	/**Number of toddler going*/
	private int toddler;
	/**Price of the tour*/
	private double price;
	/**Total Fee of the tour (including tour fee and service charge
	 * @see #price*/
	private double totalFee;
	/**Special Request of the customer*/
	private String request;
	/**To search for the correct capacity corresponding to the date*/
	private int index;
	/**Date format: d/MM*/
	private DateFormat df = new SimpleDateFormat("d/MM");
	/**Price Format: to show 1 decimal place only*/
	private DecimalFormat priceFormat = new DecimalFormat("#0.#");

	public Booking() {}

	/** This method asks customer for booking details, verify the details and
	 * if the details are correct, tour will be booked.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer*/
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception{

		ArrayList<Message> messages = new ArrayList<Message>();
		booking = customer.getBooking();
		tour = booking.getTour();
		lineID = booking.getLineID();
		chosenDate = booking.getDate();
		adult = booking.getNoOfAdults();
		children = booking.getNoOfChildren();
		toddler = booking.getNoOfToddler();
		price = booking.getTourFee();
		totalFee = booking.getTotalFee();
		request = booking.getSpecialRequest();
		index = booking.getIndexCapacity();	

		if (tokens[0].equals("no")) {
			customer.setStatus(-2);
			booking.clearAllData();
			customer.setBooking(booking);
			TextMessage message1 = new TextMessage("You have abandoned this booking process.");
			TextMessage message2 = new TextMessage("What other help do you need?");
			messages.add(message1);
			messages.add(message2);
			return messages;
		}
		else if (totalFee != -1 && tokens[0].equals("confirmed")) {
			java.sql.Date sqlDate = new java.sql.Date(chosenDate.getTime());
			if (tour instanceof DiscountedTour) {
				Tour discountedTour = database.checkTourDiscount();
				if (discountedTour == null || !(discountedTour.getTourID().equals(tour.getTourID())) || (discountedTour.getAvailableDate().get(0).compareTo(sqlDate) != 0)) {			
					TextMessage message = new TextMessage("Sorry, discounted tickets are sold out.");
					messages.add(message);
					return messages;
				}
			}

			boolean success;
			if (tour instanceof DiscountedTour) { 
				success = database.bookTour(lineID, tour.getTourID(), sqlDate, adult, children, toddler, 0, totalFee, request, true);
			}
			else {
				success = database.bookTour(lineID, tour.getTourID(), sqlDate, adult, children, toddler, 0, totalFee, request, false);
			}		
			TextMessage message1;
			TextMessage message2;
			if (success) {
				if (tour instanceof DiscountedTour) {
					database.updateDiscountedCustomer(tour.getTourID(), sqlDate);
				}
				message1 = new TextMessage("The booking process is done. Thank you for booking " + tour.getName() + "!");
				message2 = new TextMessage(CONFIRMED_BOOKING_PAYMENT);
			}
			else {
				message1 = new TextMessage("Some errors occur during the booking process.");
				message2 = new TextMessage("Please contact our customer service for help.");
			}

			TextMessage message3 = new TextMessage("What other help do you need?");
			customer.setStatus(-2);
			booking.clearAllData();
			customer.setBooking(booking);
			messages.add(message1);
			messages.add(message2);
			messages.add(message3);
			return messages;
		}

		if (lineID == null) {
			booking.setLineID(customer.getLineID());
			customer.setBooking(booking);			
		}

		if (price == -1 && chosenDate != null) {
			booking.setTourFee();
			customer.setBooking(booking);
		}

		if(chosenDate == null) { 		
			for(int i = 0; i < tour.getAvailableDate().size(); i++) {
				if (text.equals(df.format(tour.getAvailableDate().get(i)))) {
					booking.setDate(tour.getAvailableDate().get(i));
					booking.setIndexCapacity(i);
					customer.setBooking(booking);
					TextMessage message = new TextMessage("How many adults?");
					messages.add(message);
					return messages;
				}
			}

			TextMessage message = new TextMessage("Invalid date. Please choose an available date.");
			messages.add(message);
			return messages;
		}
		else if (adult == -1 && (Integer.parseInt(text) >= 0 && Integer.parseInt(text) <= tour.getCapacity().get(index))) {
			booking.setNoOfAdults(Integer.parseInt(text));
			customer.setBooking(booking);
			TextMessage message = new TextMessage("How many children (Age 4-11)?");
			messages.add(message);
			return messages;
		}
		else if (adult == -1 && !(Integer.parseInt(text) >= 0 && Integer.parseInt(text) <= tour.getCapacity().get(index))) {
			TextMessage message1 = new TextMessage("Sorry! There is only " + String.valueOf(tour.getCapacity().get(index)) + " vacancy.");
			TextMessage message2 = new TextMessage("Do you want to continue this booking? (If yes, please re-enter the number of adults. Otherwise, type 'no'.)");
			messages.add(message1);
			messages.add(message2);
			return messages;
		}
		else if (children == -1 && (Integer.parseInt(text) >= 0 && Integer.parseInt(text) <= tour.getCapacity().get(index) - adult)) {
			booking.setNoOfChildren(Integer.parseInt(text));
			customer.setBooking(booking);
			TextMessage message = new TextMessage("How many toddler (Age 0-3)?");
			messages.add(message);
			return messages;
		}
		else if (children == -1 && !(Integer.parseInt(text) >= 0 && Integer.parseInt(text) <= tour.getCapacity().get(index) - adult)) {
			TextMessage message1 = new TextMessage("Sorry! There is only " + String.valueOf(tour.getCapacity().get(index)-adult) + " vacancy.");
			TextMessage message2 = new TextMessage("Do you want to continue this booking? (If yes, please re-enter the number of children. Otherwise, type 'no'.)");
			messages.add(message1);
			messages.add(message2);
			return messages;
		}
		else if (toddler == -1 && (Integer.parseInt(text) >= 0 && Integer.parseInt(text) <= tour.getCapacity().get(index) - adult - children)) {
			if (booking.getTour() instanceof DiscountedTour) {
				java.sql.Date sqlDate = new java.sql.Date(chosenDate.getTime());
				if ((adult + children + Integer.parseInt(text)) > 2 || database.checkDiscountExceedLimit(customer.getCustomerID(), tour.getTourID(), sqlDate, (adult + children + Integer.parseInt(text)))) {
					booking.setNoOfAdults(-1);
					booking.setNoOfChildren(-1);
					customer.setBooking(booking);
					TextMessage message1 = new TextMessage("Sorry! Each customer can only reserve 2 discounted tickets.");
					TextMessage message2 = new TextMessage("Do you want to continue this booking? (If yes, please re-enter the number of adults. Otherwise, type 'no'.)");
					messages.add(message1);
					messages.add(message2);
					return messages;
				}
			}
			if ((adult + children + Integer.parseInt(text)) <= 0) {
				booking.setNoOfAdults(-1);
				booking.setNoOfChildren(-1);
				customer.setBooking(booking);
				TextMessage message1 = new TextMessage("Invalid number of tickets.");
				TextMessage message2 = new TextMessage("Do you want to continue this booking? (If yes, please re-enter the number of adults. Otherwise, type 'no'.)");
				messages.add(message1);
				messages.add(message2);
				return messages;
			}

			booking.setNoOfToddler(Integer.parseInt(text));
			customer.setBooking(booking);
			TextMessage message = new TextMessage("Do you have any special request? (If yes, please enter your request. Otherwise, type 'NULL')");
			messages.add(message);
			return messages;
		}
		else if (toddler == -1 && !(Integer.parseInt(text) >= 0 && Integer.parseInt(text) <= tour.getCapacity().get(index) - adult - children)) {
			TextMessage message1 = new TextMessage("Sorry! There is only " + String.valueOf(tour.getCapacity().get(index)-adult-children) + " vacancy.");
			TextMessage message2 = new TextMessage("Do you want to continue this booking? (If yes, please re-enter the number of toddler. Otherwise, type 'no'.)");
			messages.add(message1);
			messages.add(message2);
			return messages;
		}
		else if (request == null && (text.length() > 0 && text.length() <= 50)) {
			booking.setSpecialRequest(text);
			int totalNoOfPeople = adult + children + toddler;
			double serviceCharge = 60.0 * tour.getDays() * totalNoOfPeople;
			double adultTotalFee = price * adult;
			double childrenTotalFee = price * 0.8 * children;
			booking.setTotalFee(adultTotalFee + childrenTotalFee + serviceCharge);
			customer.setBooking(booking);
			String line1 = tour.getTourID() + " " + tour.getName() + " " + df.format(chosenDate);
			String line2 = "\nNo. of adults: " + String.valueOf(adult) + " * $" + priceFormat.format(price) + " = $" + priceFormat.format(adultTotalFee);
			String line3 = "\nNo. of children: " + String.valueOf(children) + " * $" + priceFormat.format(price*0.8) + " = $" + priceFormat.format(childrenTotalFee);
			String line4 = "\nNo. of toddler: " + String.valueOf(toddler) + " (free of charge)";
			String line5 = "\nService Charge: $60 * " + String.valueOf(totalNoOfPeople) + " people * " + String.valueOf(tour.getDays()) + " days = $" + priceFormat.format(serviceCharge);
			String line6 = "\nTotal fee: $" + priceFormat.format(adultTotalFee + childrenTotalFee + serviceCharge);
			String line7 = "\n\nSpecial request: " + booking.getSpecialRequest();
			TextMessage message1 = new TextMessage(line1 + line2 + line3 + line4 + line5 + line6 + line7);
			TextMessage message2 = new TextMessage("Do you wish to confirm this booking? (If yes, please enter 'confirmed'. Otherwise, enter 'no'.)");
			messages.add(message1);
			messages.add(message2);
			return messages;
		}

		TextMessage message = new TextMessage("Sorry I don't understand what you are saying.");
		messages.add(message);
		return messages;
	}
}


