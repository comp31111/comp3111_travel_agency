package com.example.bot.spring;

import java.util.ArrayList;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import lombok.extern.slf4j.Slf4j;

/** A class to greet the customer
 * @see FunctionStrategy*/
@Slf4j
public class Greeting implements FunctionStrategy {
	
	/** This method greets the customer politely.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer*/
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) {
		ArrayList<Message> messages = new ArrayList<Message>();
		TextMessage message = new TextMessage("Welcome! How can I help you?");
		messages.add(message);
		return messages;
	}
}