package com.example.bot.spring;

import java.util.ArrayList;
import com.linecorp.bot.model.message.Message;
/** An interface for different operations*/
public interface FunctionStrategy {
	/** This method responds to customers for different operations.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer*/
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception;

}