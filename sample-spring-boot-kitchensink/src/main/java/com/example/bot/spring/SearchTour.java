package com.example.bot.spring;

import java.util.ArrayList;
import java.util.Date; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.lang.NumberFormatException;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import lombok.extern.slf4j.Slf4j;

/** A class searches tours with specific requirements for customer from database
 * @see FunctionStrategy*/

@Slf4j
public class SearchTour implements FunctionStrategy {

	/** Database engine */
	private SQLDatabaseEngine database = new SQLDatabaseEngine();
	/** List of searched tours */
	private ArrayList<Tour> tourArrTemp;
	/** Index of the searched tours */
	private int tourNo = -1;
	/** Adjective describing tours that customers want to search */
	private String adj = "";
	/** Verb describing tours that customers want to search */
	private String verb = "";
	/** Time of tours that customers want to search */
	private String time = "";
	/** Place of tours that customers want to search */
	private String place = "";
	/** Hotel that customers want to search */
	private String chosenHotel = "";
	/** Duration of tours that customers want to search */
	private String numOfDays = "";
	/** Departure date of tours that customers want to search */
	private String departureDate = "";
	/** Price of tours that customers want to search */
	private String price = "";
	/** List of numbrs for searching */
	private String[] number = {"one", "two", "three", "four", "five", "six", "seven"};
	/** List of months for searching */
	private String[] month = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
	/** List of question verbs for searching */
	private String[] questionVerb = {"ask", "know", "join", "book", "search", "find", "choose", "go", "travel", "have"};

	/**
	 * This method is used to search tours and display the similar tours if they existed in the database.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using OpenNLP
	 * @param tags part of speech of each token
	 * @param customer the customer that is searching tour
	 * @return List of messages to reply to customer 
	**/
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception {
		
		ArrayList<Message> messages = new ArrayList<Message>();
		InProgressBooking booking = customer.getBooking();
		tourArrTemp = customer.getTourArr();

		//find the chosen tour number
		if (tags[0].equals("LS") || tags[0].equals("CD")){
			try {
				tourNo = Integer.parseInt(tokens[0]);
			} catch (NumberFormatException e) {
				for(int j = 0; j < number.length; j++) {
					if (tokens[0].equals(number[j])) {
						tourNo = j + 1;
						break;
					}
				}
			}
		}

		//display the chosen tour detail
		if(tourNo > 0 && tourNo <= tourArrTemp.size()) {
			return displayTourDetail(booking, customer, messages);
		}

		//check whether the customer wants to book the chosen tour
		if(booking.getTour() != null) { 
			return chooseTour(booking, customer, tokens, messages);
		}

		//quit this function if users don't want to search tour
		if ((booking.getTour() == null && tokens[0].equals("quit")) || (tokens[0].equals("no") && customer.getTourListIndex() != -1)) {
			customer.setTourListIndex(-1);
			customer.setStatus(-2);
			clearInstance();
			TextMessage message = new TextMessage("How can I help you?");
			messages.add(message);
			return messages;
		}

		//continue displaying the remaining tours
		if (tokens[0].equals("yes") && customer.getTourListIndex() != -1) {
			return displayTour(customer, messages);
		}

		identifyKeyword(text, tokens, tags);

		//search the database to find similar tours
		if((!adj.equals("") || !time.equals("") || !place.equals("") || !chosenHotel.equals("") || !numOfDays.equals("") || !departureDate.equals("") || !price.equals(""))) {
			
			String[] keyword = {adj, time, place, chosenHotel, numOfDays, departureDate, price};
			for (int i = 0; i < keyword.length; i++) {
				log.info(String.valueOf(i+1) + " : {}", keyword[i]); //////////////////////////////////////////////////////////////debug
			}

			customer.setTourListIndex(-1);
			tourArrTemp = database.searchTour(keyword);
			customer.setTourArr(tourArrTemp);

			if (tourArrTemp.size() == 0) {
				TextMessage message = new TextMessage("Sorry we currently do not provide such tour. Please search for other tours.\nType 'quit' if you don't want to search tours.");
				messages.add(message);
				return messages;
			}

			return displayTour(customer, messages);
		}
		else if (text.contains("tour") && (adj.equals("") && time.equals("") && place.equals("") && chosenHotel.equals("") && numOfDays.equals("") && departureDate.equals("") && price.equals(""))) {
			TextMessage message = new TextMessage("What kind of tours would you like to " + verb +"?");
			messages.add(message);
			return messages;
		}

		//error message
		TextMessage message = new TextMessage("Sorry I don't understand what you are saying.");
		messages.add(message);
		return messages;
	}

	/**
	 * This method is used to display tours in a specified format.
	 * @param customer the customer that is searching tour
	 * @param messages the returned tours
	 * @return ArrayList<Message> This returns a list of similar tours.
	**/
	private ArrayList<Message> displayTour(Customer customer, ArrayList<Message> messages) {

		int tourListIndex = customer.getTourListIndex();
		
		//display tours in this format if the result set is less than 5
		if (tourArrTemp.size() < 5) {
			TextMessage totalNo = new TextMessage("Yes. We have " + tourArrTemp.size() + " similar tours.\nPlease enter the number to view the tour details.");
			messages.add(totalNo);
			for(int i = 0; i < tourArrTemp.size(); i++) {
				TextMessage message = new TextMessage(String.valueOf(i+1) + ". " + tourArrTemp.get(i).getTourID() + " " + tourArrTemp.get(i).getName());
				messages.add(message);
			}
			return messages;
		}
		
		//display tours in this format if the result set is more than or equal to 5
		for(int i = tourListIndex + 1; i < tourArrTemp.size(); i++) {
			if (i == 0) {
				TextMessage totalNo = new TextMessage("Yes. We have " + tourArrTemp.size() + " similar tours.\nPlease enter the number to view the tour details.");
				messages.add(totalNo);
			}

			TextMessage message = new TextMessage(String.valueOf(i+1) + ". " + tourArrTemp.get(i).getTourID() + " " + tourArrTemp.get(i).getName());
			messages.add(message);

			if ((i + 2) % 4 == 0 && (i + 1) != tourArrTemp.size()) {
				TextMessage continueMsg = new TextMessage("Do you wish to list the remaining similar tour(s)? (Yes/No)");
				messages.add(continueMsg);
				customer.setTourListIndex(i);
				break;
			}

			if((i + 1) == tourArrTemp.size()) {
				customer.setTourListIndex(-1);
			}
		}

		return messages;
	}

	/**
	 * This method is used to display the chosen tour in details.
	 * @param booking used for setting the chosen tour (if any) of customer for later use
	 * @param customer the customer that is searching for tours
	 * @param messages the returned tours
	 * @return ArrayList<Message> This returns a list of messages of the tour details.
	**/
	private ArrayList<Message> displayTourDetail(InProgressBooking booking, Customer customer, ArrayList<Message> messages) {

		Tour tour = tourArrTemp.get(tourNo-1);
		TextMessage message1 = new TextMessage(tour.getTourID() + " " + tour.getName() + " * " + tour.getDescription());
		messages.add(message1);

		DateFormat df = new SimpleDateFormat("d/MM");
		DecimalFormat priceFormat = new DecimalFormat("#0.#");

		//get all confirmed date	
		String confirmedDateString = "";
		ArrayList<Date> confirmedDate = tour.getConfirmedDate();
		for(int i = 0; i < confirmedDate.size(); i++) {
			if (i == confirmedDate.size() - 1) 
				confirmedDateString = confirmedDateString + df.format(confirmedDate.get(i));
			else
				confirmedDateString = df.format(confirmedDate.get(i)) + ", " + confirmedDateString;
		}

		if (confirmedDate.size() != 0) {
			TextMessage message2 = new TextMessage("We have confirmed tour on " + confirmedDateString);
			messages.add(message2);
		}
		
		//get all available date
		String availableDateString = "";
		ArrayList<Date> availableDate = tour.getAvailableDate();
		for(int i = 0; i < availableDate.size(); i++) {
			if (i == availableDate.size() - 1)
				availableDateString = availableDateString + df.format(availableDate.get(i));
			else
				availableDateString = df.format(availableDate.get(i)) + ", " + availableDateString;
		}

		if (availableDate.size() == 0) {
			booking.clearAllData();
			customer.setBooking(booking);
			customer.setTourListIndex(-1);
			TextMessage message6 = new TextMessage("All tours of " + tour.getTourID() + " are full. Maybe you can search for another tour.\nType 'quit' if you don't want to search tours.");
			messages.add(message6);
		}
		else {
			TextMessage message3 = new TextMessage("We have tour on " + availableDateString + " still accept application");
			TextMessage message4;
			if (tour.getWeekdayPrice() != tour.getWeekendPrice()) {
				message4 = new TextMessage("Fee: Weekday $" + priceFormat.format(tour.getWeekdayPrice()) + " / Weekend $" + priceFormat.format(tour.getWeekendPrice()));
			}
			else {
				message4 = new TextMessage("Fee: $" + priceFormat.format(tour.getWeekdayPrice()));
			}				
			TextMessage message5 = new TextMessage("Do you want to book or save this tour? (Book/Save/No)");
			messages.add(message3);
			messages.add(message4);
			messages.add(message5);
			booking.setTour(tour);
			customer.setBooking(booking);
		}

		clearInstance();
		return messages;
	}

	/**
	 * This method is used to check whether the customer wants to book the chosen tour.
	 * If yes, check whether the customer has booked before. If the customer has not 
	 * booked before, directly he/she to CreateCustomer().
	 * If no, the customer can search or choose other tours.
	 * @param booking used for setting the chosen tour (if any) of customer for later use
	 * @param customer the customer that is searching for tours
	 * @param tokens all English words from text by splitting it using OpenNLP
	 * @param messages the returned tours
	 * @return ArrayList<Message> This returns the corresponding respond according to the user input.
	**/
	private ArrayList<Message> chooseTour(InProgressBooking booking, Customer customer, String[] tokens, ArrayList<Message> messages) {
		
		if (tokens[0].equals("book")) {
			TextMessage message = new TextMessage("Thank you for choosing " + booking.getTour().getTourID() + " " + booking.getTour().getName() + "!");
			messages.add(message);

			if (customer.getCustomerID() == null) {
				customer.setStatus(5);
				TextMessage message1 = new TextMessage("You have not booked any tours before. Please enter your personal information.");
				TextMessage message2 = new TextMessage("What is your name?");
				messages.add(message1);
				messages.add(message2);
				return messages;
			}
			else{
				customer.setStatus(3);
				TextMessage message1 = new TextMessage("Which date you are going?\n(Please enter in D/M format)");
				messages.add(message1);
				return messages;
			}
		}
		else if (tokens[0].equals("save")) {
			boolean success = database.addSavedTour(customer.getCustomerID(), booking.getTour().getTourID());
			if (success) {
				TextMessage message1 = new TextMessage("You have successfully saved the tour.");
				messages.add(message1);
			}
			else {
				TextMessage message1 = new TextMessage("You have already saved this tour before.");
				messages.add(message1);
			}

			TextMessage message2 = new TextMessage("You can still choose other tours from the above list or search for another tour.\nType 'quit' if you don't want to search tours.");
			messages.add(message2);
			booking.clearAllData();
			customer.setBooking(booking);
			return messages;
		}
		else if (tokens[0].equals("no")) {
			booking.clearAllData();
			customer.setBooking(booking);
			TextMessage message = new TextMessage("You can choose other tours from the above list or search for another tour.\nType 'quit' if you don't want to search tours.");
			messages.add(message);
			return messages;
		}

		TextMessage message = new TextMessage("Sorry I don't understand what you are saying.");
		messages.add(message);
		return messages;
	}

	/**
	 * This method is used to clear all the data of the class instance.
	**/
	private void clearInstance() {
		tourNo = -1;
		adj = "";
		verb = "";
		time = "";
		place = "";
		chosenHotel = "";
		numOfDays = "";
		departureDate = "";
		price = "";
	}

	/**
	 * This method searches for specific requirements of tours that customer want to search
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using OpenNLP
	 * @param tags part of speech of each token
	**/
	private void identifyKeyword(String text, String[] tokens, String[] tags) {
		//find the verb
		for(int i = 0; i < tokens.length; i++) {
			if (verb.equals("")) {
				for(int j = 0; j < questionVerb.length; j++) {
					if (tokens[i].equals(questionVerb[j])) {
						verb = tokens[i];
						break;
					}
				}
			} else break;
		}

		//search for adjectives / time / place / hotel / days / departure date / price 

		//find the month
		for (int i = 0; i < tokens.length; i++) {
				for (int j = 0; j < month.length; j++) {
					if (tokens[i].equals(month[j])) {
						time = month[j];
						break;
					}
				}

		}

		//find the place
		for(int i = 0; i < tokens.length; i++) {
			if (place.equals("") && ((tokens[i].contains("go") && !text.contains("tour"))|| tokens[i].contains("travel"))) { //I want to join tours in nov / shenzhen - || tokens[i].equals("in")
				if (tokens[i+1].equals("to")) {
					place = tokens[i+2];
					break;
				}
			}
		}

		//find the hotel
		for(int i = 0; i < tokens.length; i++) {
			if (tokens[i].contains("stay")) {
				for(int j = i + 1; j < tokens.length; j++) {
					if (tokens[j].contains("hotel")) {
						break;
					}

					if (!tags[j].equals("IN") && !tags[j].equals("DT")) {
						chosenHotel = chosenHotel + tokens[j] + " ";
					}
				}
				break;
			}
		}

		//find the days of tour
		for(int i = 0; i < tokens.length; i++) {
			if (numOfDays.equals("") && tokens[i].contains("day")) {
				numOfDays = tokens[i-1];
				break;
			}
		}

		//find the price
		for(int i = 0; i < tokens.length; i++) {
			if ((text.contains("price") || tags[i].equals("$")) && (tags[i+1].equals("CD"))) {
				price = tokens[i+1];
				log.info("find the price");
				break;
			}
		}

		//find the adjectives
		for(int i = 0; i < tokens.length; i++) {
			if (tokens[i].contains("tour") || tokens[i].contains("trip")) { //I want to join a one day tour
				for(int j = i - 1; j >= 0; j--) {
					if ((tags[j].contains("JJ") || tags[j].contains("NN") || tags[j].contains("RB")) && !tokens[j].equals(verb)) {
						adj = " " + tokens[j] + adj;
					}
					else break;
				}
			}
		}

		
	}


}