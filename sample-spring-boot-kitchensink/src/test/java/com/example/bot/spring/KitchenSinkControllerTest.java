package com.example.bot.spring;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

import static java.util.Collections.singletonList;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;

//@RunWith(SpringRunner.class)

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class KitchenSinkControllerTest {
    @Mock
    private LineMessagingClient lineMessagingClient;
    @InjectMocks
    private KitchenSinkController controller;

    private String[] test1 = {"Hi", "Welcome! How can I help you?"};
    private String[] test2 = {"Bye", "Thank you for using our service. Bye!"};

    private String[] test3 = {"How to apply?", "Customers shall approach the company by phone or visit our store (in Clearwater bay) with the choosen tour code and departure date. If it is not full, customers will be advised by the staff to pay the tour fee. Tour fee is non refundable. Customer can pay their fee by ATM to 123-345-432-211 of ABC Bank or by cash in our store. Customer shall send their pay-in slip to us by email or LINE."};
    private String[] test4 = {"What if the tour is cancelled?", "In case a tour has not enough people or bad weather condition and the tour is forced to cancel, customers will be informed 3 days in advanced. Either change to another tour or refund is avaliable for customers to select. However, due to other reasons such as customers' personal problem no refund can be made."};
    private String[] test5 = {"Are there any additional charge?", "Each customer need to pay an additional service charge at the rate $60/day/person, on top of the tour fee. It is collected by the tour guide at the end of the tour."};
    private String[] test6 = {"What is the transportation in Guangdong?", "A tour bus"};
    private String[] test7 = {"How can I contact the tour guide?", "Each tour guide has a LINE account and he will add the customers as his friends before the departure date. You can contact him/her accordingly."};
    private String[] test8 = {"Is insurance covered?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test9 = {"Is the hotel single bed or twin bed or double bed?", "All rooms are twin beds. Every customer needs to share a room with another customer. If a customer prefer to own a room by himself/herself, additional charge of 30% will be applied."};
    private String[] test10 = {"Do I need swimming suit in a water them park or a hot spring resort?", "You need swimming suit in a water theme park or a hot spring resort. Otherwise you may not use the facility."};
    private String[] test11 = {"Do you serve vegetarian?", "Sorry, we don't serve vegetarian."};
    private String[] test12 = {"How much is the tour fee for children?", "Age below 3 (including 3) is free. Age between 4 to 11 (including 4 and 11) has a discount of 20% off. Otherwise full fee applies. The same service charge is applied to all age customers."};
    private String[] test13 = {"What if I am late in the departure date?", "You shall contact the tour guide if you know you will be late and see if the tour guide can wait a little bit longer. No refund or make up shall be made if a customer is late for the tour."};
    private String[] test14 = {"Visa problem", "Please refer the Visa issue to the immigration department of China. The tour are assembled and dismissed in mainland and no cross-border is needed. However, you will need a travelling document when you check in the hotel."};
    private String[] test15 = {"What is the surcharge?", "Each customer need to pay an additional service charge at the rate $60/day/person, on top of the tour fee. It is collected by the tour guide at the end of the tour."};
    private String[] test16 = {"What is the tour fee for children?", "Age below 3 (including 3) is free. Age between 4 to 11 (including 4 and 11) has a discount of 20% off. Otherwise full fee applies. The same service charge is applied to all age customers."};
    private String[] test17 = {"How to cancel a tour?", "In case a tour has not enough people or bad weather condition and the tour is forced to cancel, customers will be informed 3 days in advanced. Either change to another tour or refund is avaliable for customers to select. However, due to other reasons such as customers' personal problem no refund can be made."};
    private String[] test18 = {"What tour guide can I find?", "Each tour guide has a LINE account and he will add the customers as his friends before the departure date. You can contact him/her accordingly."};
    private String[] test19 = {"Can I talk to the tour guide?", "Each tour guide has a LINE account and he will add the customers as his friends before the departure date. You can contact him/her accordingly."};
    private String[] test20 = {"Could I reach the tour guide?", "Each tour guide has a LINE account and he will add the customers as his friends before the departure date. You can contact him/her accordingly."};
    private String[] test21 = {"What insurance do you provide?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test22 = {"How is the insurance policy?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test23 = {"Do insurance cover?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test24 = {"Does insurance cover?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test25 = {"Will insurance be covered?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test26 = {"Would insurance be covered?", "Insurance is covered. Each customers are protected by the Excellent Blue-Diamond Scheme by AAA insurance company."};
    private String[] test27 = {"Does the tour serve vegetarian?", "Sorry, we don't serve vegetarian."};
    private String[] test28 = {"Will you serve vegetarian?", "Sorry, we don't serve vegetarian."};
    private String[] test29 = {"Would the tour serve vegetarian?", "Sorry, we don't serve vegetarian."};
    private String[] test30 = {"what is the maximum capacity for a tour?", "Sorry we currently do not have this answer."};

    @Test
    public void handleTextMessageEvent1() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test1[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test1[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent2() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test2[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test2[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent3() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test3[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test3[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent4() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test4[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test4[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent5() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test5[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test5[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent6() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test6[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test6[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent7() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test7[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test7[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent8() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test8[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test8[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent9() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test9[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test9[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent10() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test10[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test10[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent11() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test11[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test11[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent12() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test12[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test12[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent13() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test13[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test13[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent14() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test14[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test14[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent15() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test15[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test15[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent16() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test16[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test16[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }





    @Test
    public void handleTextMessageEvent17() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test17[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test17[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent18() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test18[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test18[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent19() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test19[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test19[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent20() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test20[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test20[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent21() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test21[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test21[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent22() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test22[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test22[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent23() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test23[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test23[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent24() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test24[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test24[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent25() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test25[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test25[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent26() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id",  test26[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test26[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent27() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test27[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test27[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent28() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test28[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test28[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent29() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test29[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test29[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }

    @Test
    public void handleTextMessageEvent30() throws Exception {

        final MessageEvent request = new MessageEvent<>(
                "replyToken",
                new GroupSource("groupId", "userId"),
                new TextMessageContent("id", test30[0]),
                Instant.now()
        );
        TextMessage msg = new TextMessage(test30[1]);
        when(lineMessagingClient.replyMessage(new ReplyMessage(
                "replyToken", msg))).thenReturn(CompletableFuture.completedFuture(
                new BotApiResponse("ok", Collections.emptyList())
        ));
        controller.handleTextMessageEvent(request);
        verify(lineMessagingClient).replyMessage(new ReplyMessage("replyToken", msg));
    }


}