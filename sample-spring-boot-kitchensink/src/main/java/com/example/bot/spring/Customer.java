package com.example.bot.spring;

import java.util.ArrayList;
/** A class to hold details of the customer */
public class Customer {
	/** Line ID of the customer */
    private String lineID;
	/** Customer ID in database */
    private String customerID;
	/** Name of customer */
    private String name;
	/** Phone number of customer */
    private String phoneNo;
	/** Age of customer */
    private int age;

	/** the current operation of the customer
	 * 0: Greeting the bot
	 * 1: Searching FAQ
	 * 2: Searching Tours
	 * 3: Booking Tour
	 * 4: Searching booking records
	 * 5: Being asked for details to create customer record in database
	 * 6: Searching Saved Tours
	 * -1: Exit */
    private int status;
	/** Current booking of customer */
    private InProgressBooking booking;
	/** List of current searched tours */
    private ArrayList<Tour> tourArrTemp;
	/** For listing tours if number of searched tours is larger than 4 */
    private int tourListIndex;
	/** For listing booking records if number of records is larger than 4 */
    private int recordListIndex;
    /** For listing saved tours if number of saved tours is larger than 4 */
    private int savedListIndex;

	/** Constructor 
	 * Status will be set to -2. Booking and tourArrTemp will be initialized.
	 * All other class fields will be set to null or -1.*/
    public Customer() {
        this.lineID = null;
        this.customerID = null;
        this.name = null;
        this.phoneNo = null;
        this.age = -1;

        this.status = -2;
        this.booking = new InProgressBooking();
        this.tourArrTemp = new ArrayList<Tour>();
        this.tourListIndex = -1;
        this.recordListIndex = -1;
        this.savedListIndex = -1;
    }

	/** Get Line ID
	 * @return Line ID
	 * @see #lineID */
    public String getLineID() { return lineID; }
	/** Get Customer ID
	 * @return Customer ID
	 * @see #customerID */
    public String getCustomerID() { return customerID; }
	/** Get name
	 * @return name
	 * @see #name */
    public String getName() { return name; }
	/** Get phone number
	 * @return phone number
	 * @see #phoneNo */
    public String getPhoneNo() { return phoneNo; }
	/** Get age
	 * @return age
	 * @see #age */
    public int getAge() { return age; }
	/** Get status
	 * @return status
	 * @see #status */
    public int getStatus() { return status; }
	/** Get current booking
	 * @return current booking
	 * @see #booking */
    public InProgressBooking getBooking() { return booking; }
	/** Get list of current searched tours
	 * @return list of current searched tours
	 * @see #tourArrTemp */
    public ArrayList<Tour> getTourArr() { return tourArrTemp; }
	/** Get the index of listing current searched tours 
	 * @return the index of listing current searched tours 
	 * @see #tourListIndex */
    public int getTourListIndex() { return tourListIndex; }
	/** Get the index of listing previous booking records
	 * @return the index of listing previous booking records
	 * @see #recordListIndex */
    public int getRecordListIndex() { return recordListIndex; }
	/** Get the index of listing saved tour records
	 * @return the index of listing saved tour records
	 * @see #savedListIndex */
    public int getSavedListIndex() { return savedListIndex; }

	/** Set customer ID
	 * @param customerID Customer ID */
    public void setCustomerID(String customerID) { this.customerID = customerID; }
	/** Set name
	 * @param name Name */
    public void setName(String name) { this.name = name; }
	/** Set phone number
	 * @param phoneNo Phone Number */
    public void setPhoneNo(String phoneNo) { this.phoneNo = phoneNo; }
	/** Set age
	 * @param age Age */
    public void setAge(int age) { this.age = age; }
	/** Set Line ID
	 * @param lineID Line ID */
    public void setLineID(String lineID) { this.lineID = lineID; }
	/** Set status
	 * @param status Status */
    public void setStatus(int status) { this.status = status; }
	/** Set current booking
	 * @param booking current booking */
    public void setBooking(InProgressBooking booking) { this.booking = booking; }
	/** Set list of current searched tours
	 * @param tourArrTemp list of current searched tours */
    public void setTourArr(ArrayList<Tour> tourArrTemp) { this.tourArrTemp = tourArrTemp; }
	/** Set the index of listing current searched tours 
	 * @param tourListIndex index of listing current searched tours  */
    public void setTourListIndex(int tourListIndex) { this.tourListIndex = tourListIndex; }
	/** Set the index of listing previous booking records 
	 * @param recordListIndex index of listing previous booking records  */
    public void setRecordListIndex(int recordListIndex) { this.recordListIndex = recordListIndex; }
	/** Set the index of listing saved tours records 
	 * @param savedListIndex index of listing saved tours records  */
    public void setSavedListIndex(int savedListIndex) { this.savedListIndex = savedListIndex; }

}