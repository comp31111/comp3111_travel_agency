package com.example.bot.spring;

import lombok.extern.slf4j.Slf4j;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import java.sql.*;
import java.net.URISyntaxException;
import java.net.URI;

import java.util.Calendar;
import java.util.Collections;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.text.DecimalFormat;
import java.io.StringWriter;
import java.io.PrintWriter;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Comparator;
import java.util.LinkedHashMap;

/** A class for calling database functions. */

@Slf4j
public class SQLDatabaseEngine {

	/** This method takes in the user input and search tours in database based on the specific requirement
	 * @param text user input
	 * @return array list of tours matching the requirements*/
	ArrayList<Tour> searchTour(String[] text) throws Exception {		//search a tour information	

		String[] number = {"one", "two", "three", "four", "five", "six", "seven"};
		String[] monthStr = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};

		String adj = "";
		if (!text[0].equals("")) {
			adj = text[0].substring(1);
		}
		
		int monthInt = -1;
		String time = "";
		if (!text[1].equals("")) {
			for(int i = 0; i < monthStr.length; i++) {
				if(monthStr[i].equals(text[1])) {
					monthInt = i + 1;
					break;
				}
			}
		}

		String place = text[2];
		
		String chosenHotel = "";
		if (!text[3].equals("")) {
			chosenHotel = text[3].substring(0, text[3].length()-1);
		}
		
		int numOfDays = -1;
		if (!text[4].equals("")) {
			try {
				numOfDays = Integer.parseInt(text[4]);
			} catch (NumberFormatException e) {
				for(int j = 0; j < number.length; j++) {
					if (text[4].equals(number[j])) {
						numOfDays = j + 1;
						break;
					}
				}
			}
		}

		String departure = text[5];

		double price = -1.0;
		if (!text[6].equals("")) {
			try {
				price = Double.parseDouble(text[6]);
			} catch (NumberFormatException e) {
				log.info("fail to convert string");
			}
		}

		int index = -1;
		ArrayList<Tour> tourArr = new ArrayList<Tour>();
		Connection connection = getConnection();
		
		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Tour T, TourDetail TD WHERE T.TID = TD.TID AND date > current_date + 3 ORDER BY T.TID, TD.Date");
		
		ResultSet rs = stmt.executeQuery();
		String previousTourID = "";
		while (rs.next()) {	
			String tourID = rs.getString(1);
			String tourName  = rs.getString(2);
			String tourDescription = rs.getString(3);
			int days = rs.getInt(4);
			String departureDate = rs.getString(5);
			double weekdayPrice = rs.getDouble(6);	
			double weekendPrice = rs.getDouble(7);					
			Date startDate = rs.getDate(10);
			String hotel = rs.getString(13);
			int capacity = rs.getInt(14);


			DateFormat df = new SimpleDateFormat("yyyyMMdd");	
			String month = df.format(startDate);
			ArrayList<java.util.Date> tourAvailableDate;
			ArrayList<java.util.Date> tourConfirmedDate;
			ArrayList<Integer> tourCapacity;
			
			//if keyword matched, get the tour information
			if ((!adj.equals("") && (tourName.toLowerCase().contains(adj.toLowerCase()) || tourDescription.toLowerCase().contains(adj.toLowerCase())))
				|| (!place.equals("") && (tourName.toLowerCase().contains(place.toLowerCase()) || tourDescription.toLowerCase().contains(place.toLowerCase())))
				|| days == numOfDays || Integer.parseInt(month.substring(4,6)) == monthInt || (!chosenHotel.equals("") && hotel.toLowerCase().contains(chosenHotel.toLowerCase()))
				|| (weekendPrice < (price + 100) && weekendPrice > (price - 100)) || (weekdayPrice < (price + 100) && weekdayPrice > (price - 100))) {
				//|| departureDate.toLowerCase().contains(text.toLowerCase())
				//|| df.format(startDate).equals(text) 
				
				if (!previousTourID.equals(tourID)){
					tourArr.add(new Tour(tourName, tourID, tourDescription, weekendPrice, weekdayPrice, days));
					++index;
				}

				tourAvailableDate = tourArr.get(index).getAvailableDate();
				tourConfirmedDate = tourArr.get(index).getConfirmedDate();
				tourCapacity = tourArr.get(index).getCapacity();
				String statement = "SELECT Date, SUM(NoOfAdults), SUM(NoOfChildren), SUM(NoOfToddler) FROM BookingRecord WHERE TID = '" + tourID + "' AND Date = '" + startDate + "' GROUP BY Date";
				PreparedStatement stmt2 = connection.prepareStatement(statement);
				ResultSet rs2 = stmt2.executeQuery();

				if(!rs2.isBeforeFirst()) {
					tourAvailableDate.add(startDate);
					tourCapacity.add(capacity);
					log.info("no record - tid: {}, date: {}, capacity: {}", tourID, df.format(startDate), String.valueOf(capacity));
				}

				while (rs2.next()) {

					int availableCapacity = capacity - (rs2.getInt(2) + rs2.getInt(3) + rs2.getInt(4));

					log.info("tid: {}, date: {}, capacity: {}", tourID, df.format(startDate), String.valueOf(availableCapacity));

					if(availableCapacity == 0) {
						tourConfirmedDate.add(startDate);
						//tourCapacity.add(0);
					}
					else {
						tourAvailableDate.add(startDate);
						tourCapacity.add(availableCapacity);
					}
				}

				previousTourID = tourID;

				if (rs2 != null) {
					rs2.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
			}			
		}

		if (rs != null) {
			rs.close();
		}
		if (stmt != null) {
			stmt.close();
		}
		if (connection != null) {
			connection.close();
		}
		
		return tourArr;		
	}

	/** This method insert a booking record for customer booking tour
	 * @param lineID Line ID of customer
	 * @param tid Tour ID
	 * @param tourDate date of going
	 * @param adult number of adults going
	 * @param children number of children going
	 * @param toddler number of toddlers going
	 * @param amountPaid the amount that customer have already paid
	 * @param tourFee total fee
	 * @param request special request of customer
	 * @param discount true if the tour is under discount, otherwise, false
	 * @return true if successfully inserted record into database, otherwise, false*/
	boolean bookTour(String lineID, String tid, Date tourDate, int adult, int children, int toddler, double amountPaid, double tourFee, String request, boolean discount) throws Exception {
		boolean success = true;
		String bid = "";
		String cid = "";
		Connection connection = getConnection();

		//get previous Booking ID
		PreparedStatement getBID = connection.prepareStatement("SELECT BID FROM BookingRecord ORDER BY BID");
		ResultSet rs = getBID.executeQuery();
		while (rs.next()) {
			bid = rs.getString(1);
		}
		bid = "B" + nextID(bid);

		//get the Customer ID
		PreparedStatement getCID = connection.prepareStatement("SELECT CID FROM Customer WHERE LineID = '" + lineID + "'");
		ResultSet rs2 = getCID.executeQuery();
		while (rs2.next()) {
			cid = rs2.getString(1);
		}		
		PreparedStatement stmt = null;
		try {
			String insertSQL = "INSERT INTO BookingRecord" + " VALUES" + "(?,?,?,?,?,?,?,?,?,?,?)";
			stmt = connection.prepareStatement(insertSQL);
			stmt.setString(1, cid);
			stmt.setString(2, tid);
			stmt.setInt(3, adult);
			stmt.setInt(4, children);
			stmt.setInt(5, toddler);
			stmt.setDouble(6, tourFee);
			stmt.setDouble(7, amountPaid);
			stmt.setString(8, request);
			stmt.setString(9, bid);
			stmt.setDate(10, tourDate);
			stmt.setBoolean(11, discount);
			stmt.executeUpdate();

		} catch (SQLException e) {
			success = false;
		} 
		if (rs2 != null)
			rs2.close();
		if (rs != null)
			rs.close();
		if (getCID != null)
			getCID.close();
		if (getBID != null)
			getBID.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		
		return success;
	}

	/** This method search database for previous booking records of a specific customer
	 * @param id Line ID of customer
	 * @return Array list of details of previous booking records of the customer*/
	
	ArrayList<String[]> searchBookingRecord(String id) throws Exception {
		ArrayList<String[]> recordArr = new ArrayList<String[]>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat priceFormat = new DecimalFormat("#0.#");
		int index = 0;

		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("SELECT T.TourName, B.* FROM BookingRecord B, Tour T, Customer C WHERE T.TID = B.TID AND C.CID = B.CID AND C.LineID = '" + id + "' ORDER BY BID DESC" );
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			String[] temp = new String[9];
			temp[0] = rs.getString(3);				    	//tour ID
			temp[1] = rs.getString(1);  					//tour name
			temp[2] = df.format(rs.getDate(11));	    	//date
			temp[3] = String.valueOf(rs.getInt(4));			//number of adults
			temp[4] = String.valueOf(rs.getInt(5));			//number of children
			temp[5] = String.valueOf(rs.getInt(6));			//number of toddler
			temp[6] = priceFormat.format(rs.getDouble(7));	//tour fee
			temp[7] = priceFormat.format(rs.getDouble(7));	//amount paid
			if (rs.getString(9).toLowerCase().equals("null")) {
				temp[8] = "N/A";
			}
			else {
				temp[8] = rs.getString(9);					//special request
			}
			recordArr.add(temp);
			index++;
		}

		if (rs != null) 
			rs.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		
		return recordArr;
	}

	/** This method checks if there are any tours that are within next 7 days and customers have fully paid
	 * the fee. 
	 * @return Array list of upcoming tours of customer*/
	
	ArrayList<String[]> getUpcomingTour() throws URISyntaxException, SQLException {
		ArrayList<String[]> upcoming = new ArrayList<String[]>();

		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("select c.lineid, t.tourname, b.date from customer c, tour t, bookingrecord b where c.cid=b.cid AND t.tid=b.tid AND b.tourfee=b.amountpaid AND b.date <= current_date + interval '7' day AND b.date >= current_date");
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			String[] temp = new String[3];
			temp[0] = rs.getString(1);			//Line ID
			temp[1] = rs.getString(2);  		//tour name
			temp[2] = rs.getString(3);	    	//date
			upcoming.add(temp);
		}

		if (rs != null)
			rs.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		
		return upcoming;
	}

	/** This method checks if customer have any outstanding fees of tours going within 3 - 7 days
	 * @return Array list of tours meeting the requirement*/
	
	ArrayList<String[]> getOutstandingFee() throws SQLException, URISyntaxException {
		ArrayList<String[]> outstanding = new ArrayList<String[]>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("SELECT c.lineid, t.tourname, (b.date-interval '3' day) as Date, (b.tourfee-b.amountpaid) as outstanding FROM customer c, tour t, bookingrecord b WHERE c.cid=b.cid AND t.tid=b.tid AND (b.tourfee-b.amountpaid)>0 AND b.date < current_date + interval '7' day AND b.date >= current_date + interval '3' day;");
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			String[] temp = new String[4];
			temp[0] = rs.getString(1);			// LineID
			temp[1] = rs.getString(2);			// Tour name
			temp[2] = df.format(rs.getDate(3));	// Tour Date
			temp[3] = rs.getString(4);			// Outstanding Fee
			outstanding.add(temp);
		}

		if (rs != null)
			rs.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		
		return outstanding;
	}

	/** This method checks if there are any saved tours by customers
	 * @return the lastest saved tours of customers*/
	
	ArrayList<String[]> getLatestSavedTour() throws URISyntaxException, SQLException {
		ArrayList<String[]> saved = new ArrayList<String[]>();
		String previousLineId = "123";

		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("SELECT c.lineid, s.saveddate, s.tid, t.tourname FROM customer c, savedtour s, tour t WHERE c.cid=s.cid AND s.tid=t.tid ORDER BY lineid, s.saveddate DESC;");
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			String[] temp = new String[4];
			temp[0] = rs.getString(1);		// Line ID
			temp[1] = rs.getString(2);		// Saved Date
			temp[2] = rs.getString(3);		// Tour ID
			temp[3] = rs.getString(4);		// Tour Name
			if ( !temp[0].equals(previousLineId) ) {
				saved.add(temp);
			}
			previousLineId = temp[0];
		}

		return saved;
	}
	/** This method checks if a customer have booked 2 tickets of the same discounted tour
	 * @param cid Customer ID in database
	 * @param tid tour ID
	 * @param date date of going
	 * @param capacity places left for this tour
	 * @return true if the customer has booked 2 tickets of the same discounted tour, otherwise, false*/
	boolean checkDiscountExceedLimit(String cid, String tid, Date date, int capacity) throws Exception {
		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("SELECT (noOfAdults+noOfChildren+noOfToddler) AS sum FROM BookingRecord WHERE CID = ? AND TID = ? AND Date = ? AND Discounted = TRUE");
		stmt.setString(1, cid);
		stmt.setString(2, tid);
		stmt.setDate(3, date);
		ResultSet rs = stmt.executeQuery();
		int sum = 0;

		if(!rs.isBeforeFirst()) {
			return false;
		}

		while (rs.next()) {
			sum = sum + rs.getInt(1);
		}

		if (sum + capacity <= 2) {
			return false;
		}
		else {
			return true;
		}
	}
	/** This method checks if customer exists in database
	 * @param lineID Line ID of customer
	 * @param customer customer object in controller
	 * @return true if customer exists in database, otherwise, false*/
	boolean checkExistingCustomer(String lineID, Customer customer) throws Exception {
		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Customer WHERE LineID = '" + lineID + "'");
		ResultSet rs = stmt.executeQuery();

		//set customer's data if user exists in the database
		if (rs.next()) {
			if ((rs.getInt(4)) != -1) {
				customer.setCustomerID(rs.getString(1));	
				customer.setName(rs.getString(2));
				customer.setPhoneNo(rs.getString(3));
				customer.setAge(rs.getInt(4));
				customer.setLineID(lineID);
				log.info("exists customer success");
				return true;
			}
		}
		
		if (rs != null)
			rs.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		log.info("exists customer fail");
		return false;
	}
	/** This method creates customer record in database if it doesn't exist.
	 * @param lineID Line ID
	 * @param customerName name of customer
	 * @param phone phone number of customer
	 * @param age age of customer
	 * @return return true if customer record is successfully created in database, otherwise, false*/
	boolean createCustomer(String lineID, String customerName, String phone, int age) throws Exception {
		boolean success = true;
		String cid = "";
		Connection connection = getConnection();
		PreparedStatement getCID = connection.prepareStatement("SELECT CID FROM Customer ORDER BY CID");
		ResultSet rs = getCID.executeQuery();
		while (rs.next()) {
			cid = rs.getString(1);
		}		
		cid = "A" + nextID(cid);
		
		PreparedStatement stmt = null;
		try {
			if (age == -1) {
				String insertSQL = "INSERT INTO Customer" + " VALUES" + "(?,?,?,?,?)";
				stmt = connection.prepareStatement(insertSQL);
			} else {
				String updateSQL = "UPDATE Customer SET cid=?, name=?, phone=?, age=? WHERE lineID=?"; 
				stmt = connection.prepareStatement(updateSQL);
			}
			
			stmt.setString(1, cid);
			stmt.setString(2, customerName);
			stmt.setString(3, phone);
			stmt.setInt(4, age);
			stmt.setString(5, lineID);
			stmt.executeUpdate();
			log.info("create customer success");

		} catch (SQLException e) {
			log.info("create customer false");
			log.info(e.getMessage());
			success = false;
		} 

		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		
		return success;
	}
	/** This method is used to generate the next customer ID
	 * @param last_id last customer ID record in the database
	 * @return next customer ID*/
	private String nextID(String last_id) {
		String result = "";
		int parseInt = Integer.parseInt(last_id.substring(1, 7));
		String temp = String.valueOf(++parseInt);
		int fillZero = 6 - temp.length();
		for (int i = 0; i < fillZero; i++) {
			result = result + "0";
		}
		return result + temp;
	}
	/** This method gets all existing Line IDs of customers in database
	 * @return Array list of customer Line IDs*/
	ArrayList<String> getExistingLineID() throws Exception {
		ArrayList<String> result = new ArrayList<String>();
		
		Connection connection = getConnection();
		String customerID = "SELECT lineid FROM customer";
		PreparedStatement stmt = connection.prepareStatement(customerID);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			result.add(rs.getString(1)); //get lineid
		}
		
		if (rs != null)
			rs.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		return result;
	}
	/** This method checks if there is any discounted tour available.
	 * @return the discounted tour (if any)*/
	DiscountedTour checkTourDiscount() throws Exception {
		DiscountedTour result = null;
		
		Connection connection = getConnection();
		String discount = "SELECT T.*, TD.date, TD.capacity, TD.discountedcustomer FROM Tour T, TourDetail TD WHERE T.TID = TD.TID AND date > current_date + 3 AND discount = true";
		PreparedStatement stmt = connection.prepareStatement(discount);
		ResultSet rs = stmt.executeQuery();
		PreparedStatement stmt2 = null;
		ResultSet rs2 = null;
		
		while (rs.next()) {
			String tourID = rs.getString(1);
			String tourName  = rs.getString(2);
			String tourDescription = rs.getString(3);
			int days = rs.getInt(4);
			double weekdayPrice = rs.getDouble(6);	
			double weekendPrice = rs.getDouble(7);					
			Date startDate = rs.getDate(9);
			int capacity = rs.getInt(10);
			int discountedCustomer = rs.getInt(11);
			
			String checkPlace = "SELECT SUM(NoOfAdults), SUM(NoOfChildren), SUM(NoOfToddler) "
			+ "FROM BookingRecord WHERE TID = '" + tourID + "' AND Date = '" + startDate + "'";
			stmt2 = connection.prepareStatement(checkPlace);
			rs2 = stmt2.executeQuery();
			int capacityLeft = capacity;
			if (rs2.next()) {
				capacityLeft = capacity - rs2.getInt(1) - rs2.getInt(2) - rs2.getInt(3);
			}
			
			if (discountedCustomer < 5 && capacityLeft > 0) {
				result = new DiscountedTour(tourName, tourID, tourDescription, weekendPrice, weekdayPrice, days, capacityLeft, startDate); //get lineid				
			} else {
				resetDiscountedTour(tourID, startDate);
			}

		}
		if (rs2 != null)
			rs2.close();
		if (rs != null)
			rs.close();
		if (stmt2 != null)
			stmt2.close();
		if (stmt != null)
			stmt.close();
		if (connection != null)
			connection.close();
		return result;
	}
	/** This method is called when there is a customer wants to book a discounted tour. It will increment
	 * the number of customer booked this discounted tour by 1.
	 * @param tourID the discounted tour ID
	 * @param date the specific date of the tour
	 * @return true if the number of customer is successfully created, otherwise, false*/
	boolean updateDiscountedCustomer(String tourID, Date date) {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = getConnection();
			String updateDiscountedCustomer = "UPDATE tourdetail SET discountedCustomer = discountedCustomer + 1 WHERE tid =? AND Date = ?";
			stmt = connection.prepareStatement(updateDiscountedCustomer);
			stmt.setString(1, tourID);
			stmt.setDate(2, date);
			stmt.executeUpdate();
		} catch (Exception e) {
			log.info(e.getMessage());
			return false;
		} 
		try {
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		
		return true;
	}
	/** This method is called when discounted tour is no longer available. It resets the tour in tour detail.
	 * @param tourID tour ID of the no longer available discounted tour
	 * @param date date of the discounted tour
	 * @return true if the tour is successfully reset, otherwise, false*/
	boolean resetDiscountedTour(String tourID, Date date) {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = getConnection();
			String updateDiscountedCustomer = "UPDATE tourdetail SET discount = false, discountedCustomer = 0 WHERE tid =? AND date = ?";
			stmt = connection.prepareStatement(updateDiscountedCustomer);
			stmt.setString(1, tourID);
			stmt.setDate(2, date);
			stmt.executeUpdate();
		} catch (Exception e) {
			log.info(e.getMessage());
			return false;
		} 
		try {
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return true;
	}
	/** This method is called when chatbot doesn't understand the questions customer asked. It inserts the 
	 * question into database and refer to customer service team to respond.
	 * @param cid Customer ID
	 * @param currentDay day of message sent
	 * @param question the question customer asked
	 * @param answered if it is answered
	 * @return true when the question is successfully inserted into the database, otherwise, false*/
	boolean insertCustomerServiceQuestions(String cid, Date currentDay, String question, boolean answered) {
		answered = false;
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			if (cid == null || currentDay == null || question == null) {
				throw new Exception("cid / currentDay / question cannot be null.");
			}
			connection = getConnection();
			String insertCid = "INSERT INTO customerservicequestions" + " VALUES" + "(?,?,?,?)";
			
			stmt = connection.prepareStatement(insertCid);
			stmt.setString(1, cid);
			stmt.setDate(2, currentDay);
			stmt.setString(3, question);
			stmt.setBoolean(4, answered);;
			stmt.executeUpdate();
			
		} catch (Exception e) {
			log.info(e.getMessage());
			return false;
		} 
		try {
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		
		return true;
	}
	/** This method recommend tours to customers based on the categories of their previous booking records.
	 * The categories are hot spring tours, sight-seeing tours, nature tours, and culture tours. Tours can
	 * be in multiple categories.
	 * @param lineID Line ID
	 * @return Array list of tour names of the recommend tours. Maximum of 2 tours will be returned.*/
	ArrayList<String> recommendTour(String lineID) {
		ArrayList<String> result = new ArrayList<String>();
		Connection connection = null;
		try {
			ArrayList<String[]> bookingRecord = searchBookingRecord(lineID);
			if (bookingRecord == null) {
				log.info("bookingRecord = null");
				return null;
			}

			ArrayList<String> bookedTourID = new ArrayList<String>();
			for (int i = bookingRecord.size() - 1; i > -1; i--) {
				bookedTourID.add(bookingRecord.get(i)[0]);
			}
			
			// Different Categories (0: HS, 1: SS, 2: N, 3: C, 4: others)
			Map<String, Integer> categories = new HashMap<String, Integer>();
			categories.put("HS", 0);
			categories.put("SS", 0);
			categories.put("N", 0);
			categories.put("C", 0);

			connection = getConnection();
			String checkType = "SELECT category FROM tour WHERE tid = ?";
			PreparedStatement stmt = connection.prepareStatement(checkType);
			for (String tourID : bookedTourID) {
				stmt.setString(1, tourID);
				ResultSet rs = stmt.executeQuery();
				String category = "";
				if (rs.next()) {
					category = rs.getString(1);
				}
				// Check for different categories
				if (category.contains("HS") || category.contains("SS") || category.contains("N") 
					|| category.contains("C")) {
					if (category.contains("HS")) {
						categories.put("HS", categories.get("HS") + 1);
					}
					if (category.contains("SS")) {
						categories.put("SS", categories.get("SS") + 1);
					}
					if (category.contains("N")) {
						categories.put("N", categories.get("N") + 1);
					}
					if (category.contains("C")) {
						categories.put("C", categories.get("C") + 1);
					}
				}
				if (rs != null)
					rs.close();
			}
			if (stmt != null)
				stmt.close();
			
			// Sort Categories
			List<Map.Entry<String, Integer>> entryList = new ArrayList<>(categories.entrySet());
			Comparator<Map.Entry<String, Integer>> sortByValue = (e1, e2)-> {return (e1.getValue().compareTo(e2.getValue())); };
			Collections.sort(entryList, sortByValue);
			Map<String, Integer> sortedCategories = new LinkedHashMap<>();
			for (Map.Entry<String, Integer> e: entryList) {
				if (e.getValue() != 0)
					sortedCategories.put(e.getKey(),e.getValue());
			}

			ArrayList<String> correctSortedCategories = new ArrayList<String>();
			List<String> keys = new ArrayList<String>(sortedCategories.keySet());
			for (int i = keys.size() - 1; i > -1; i--) {
				correctSortedCategories.add(keys.get(i));
			}
			
			// Find suitable tour
			for (int i = correctSortedCategories.size() - 1; i > -1; i--) {
				if (result.size() > 1) {
					break;
				}
				String selectSuitableTour = "SELECT DISTINCT(T.tourName) FROM tour T, tourdetail TD WHERE T.tid = TD.tid"
				+ " AND T.category LIKE '%" + correctSortedCategories.get(i) + "%' AND date > current_date + 3";
				for (String tourID: bookedTourID) {
					selectSuitableTour = selectSuitableTour + " AND T.tid <> '" + tourID + "'";
				}
				ResultSet rs2 = connection.prepareStatement(selectSuitableTour).executeQuery();
				
				if (rs2.next()) {
					String tourName = rs2.getString(1);
					result.add(tourName);
				}
			}
			if (result.size() == 0 || result == null) {
				return null;
			}
			
		} catch (Exception e) {
			log.info(e.getMessage());
		} 
		try {
			if (connection != null) 
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		
		return result;

	}
	/** This method finds newly inserted tours in database.
	 * @return Array list of newly opened tours*/
	ArrayList<String[]> findNewTour() {
		ArrayList<String[]> result = new ArrayList<String[]>();
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String findNewTour = "SELECT t.tourname, td.date FROM tour t, tourdetail td WHERE t.tid = td.tid "
			+ "AND datecreated = current_date";
			stmt = connection.prepareStatement(findNewTour);
			rs = stmt.executeQuery();
			while(rs.next()) {
				if (result.size() < 5) {
					String[] temp = new String[2];
					temp[0] = rs.getString(1);
					temp[1] = rs.getString(2);
					result.add(temp);
				}
			}
			
		} catch(Exception e) {
			log.info(e.getMessage());
		}
		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
			
		}
		return result;
	}
	/** This method adds a tour into customers' saved tour records.
	 * @param cid customer ID
	 * @param tid tour ID
	 * @return true if the tour is successfully saved into customers' saved tour records, otherwise, false*/
	boolean addSavedTour(String cid, String tid) {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = getConnection();
			stmt = connection.prepareStatement("insert into savedtour values ('" + cid + "', '" + tid + "', current_date)");
			stmt.executeUpdate();
		} catch (Exception e) {
			return false;
		}

		try {
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}

		return true;
	}
	/** This method checks if customer have any saved tours
	 * @param cid customer ID
	 * @return return the saved tours of customers if any*/
	ArrayList<String[]> getSavedTour(String cid) throws Exception {
		ArrayList<String[]> savedTour = new ArrayList<String[]>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Connection connection = getConnection();
		PreparedStatement stmt = connection.prepareStatement("select t.tid, t.tourname, s.saveddate from savedtour s, tour t where s.tid = t.tid and s.cid = '" + cid + "' ORDER BY s.saveddate desc");
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			String[] temp = new String[3];
			temp[0] = rs.getString(1);	//tid
			temp[1] = rs.getString(2);	//tourname
			temp[2] = df.format(rs.getDate(3));	//saveddate
			savedTour.add(temp);
		}
		try {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
			if (connection != null)
				connection.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return savedTour;
	}
	/** This method gets connection to database.
	 * @return connection to database*/
	private Connection getConnection() throws URISyntaxException, SQLException {
		Connection connection;
		URI dbUri = new URI(System.getenv("DATABASE_URL"));

		String username = dbUri.getUserInfo().split(":")[0];
		String password = dbUri.getUserInfo().split(":")[1];
		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() +  "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";

		log.info("Username: {} Password: {}", username, password);
		log.info ("dbUrl: {}", dbUrl);

		connection = DriverManager.getConnection(dbUrl, username, password);
		return connection;
	}
}
