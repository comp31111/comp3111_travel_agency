# README #

## General Requirements ##

- Greet a customer politely.
- Answer FAQ (provided in separated sheet).
- Handle tour enquiry with live data.
- Calculate tour fee for the customer.
- Allow the customer to book a tour on the bot

## Guidelines ##
- Features > Should contain at least 8 features.
- Interface > At least, do something like Auto-answer machine, e.g. For Tour Information, press 1; For Tour Price, press 2
- Technology > Can use other API/framework as long as it is under licence and you have cited it.
- Test Vectors > Create your own test cases. The more comprehensive the better.
- Working Environment > Heroku, Java Spring (optionally Spring Boot), Version Control, Project Management Software

## Milestone 1 (Due: 12 Oct 2017): ##

###Students should submit:###
- Meeting record of interviewing clients (meeting minutes/tabular requirements) (For Topic 1 only)
- Related references gathered from the web
- Proposal of features and functions in the chatbot.

###TA will feedback:###
- Score based on the fulfillment of each item (see marking rubrics)
- Informal comments of the submit items.
- Sign-off the proposed features.

## Milestone 2 (Due: 3 Nov 2017): ##

###Students should submit:###
- A running prototype on LINE that has implemented at least half of the features.
- Architecture design presented in UML
- Evidence of pull request, merge and commit log messages
- Formal approval from the mock client of change of feature, if any

###TA will feedback:###
- Score based on the fulfillment of each item (see marking rubrics)
- A new requirement from the client/the marketing department.

## Milestone 3 (Due: 20 Nov 2017): ##

###Student should submit:###
- A bitbucket repository containing their code.
- Rational of change of architecture design, if any
- A LINE bot account ID that allows TA to test and grade.
- Test suites and the result.
- Documentation of their code (Javadoc)
- Evident of using project management software (e.g. Trello)
- Evident of fulfillment of the proposed feature
- Evident of fulfillment of the new client's requirement

###TA will feedback:###
- Score based on the fulfillment of each item (see marking rubrics)

## Final Presentation (Week 13 during the lecture time) (the format and details are to be finalized): ##

###Student should:###
- Prepare a one-page poster (format PDF, orientation: landscape, to be projected on screen) that describes their project features and how it meets the requirements.
- Present their project within 5-7 minutes on stage.
- Describe how the software development techniques are applied in the project.
- Pick one or two features and describe its technical difficulty.
- Answer questions raised by the instructor and TAs.

###Instructor and TA will feedback:###
- Score based on the fulfillment of each item (see marking rubrics)

## Userful Materials ##
- Project Spec
	https://github.com/khwang0/2017F-COMP3111/blob/master/ProjectSpec.md

- Project Guideline
	https://github.com/khwang0/2017F-COMP3111/blob/master/Project/topic%201/projectGuideline.md

- Tour Application Flow Chart
	https://github.com/khwang0/2017F-COMP3111/blob/master/Project/topic%201/Tour%20Application%20Flow%20chart.pdf

- Sample Dialog
	https://github.com/khwang0/2017F-COMP3111/blob/master/Project/topic%201/sample-dialog.md

- FAQ
	https://github.com/khwang0/2017F-COMP3111/blob/master/Project/topic%201/faq.md

- Others
	https://github.com/khwang0/2017F-COMP3111/tree/master/Project/topic%201

## OUR FEATURES ##

#### Enquiry ####
- Respond customers in a polite manner
- Handle customers' general queries from a pre-defined question pool AND Refer customers to our customer service team if they need further assistance
- Handle customers’ inquiries about specific tour with live data, such as tour price and remaining quota
- List out all the surcharges/additional fees included in thetotal tour fee, such as service charge
- List out previous booking records for customers to review
#### Booking ####
- Gather the required information, such as name, phone number, age, number of adults and children, etc., to book a tour
- Allow customers to save tours in case they want to book later
#### Promotion ####
- Recommend tours to customers, based on their previous booking records
- Inform customers of newly opened tour AND newly released promotion campaign
#### Reminder ####
- Remind customers of their upcoming joined tours
- When the remaining quota of a specific tour is less than a certain number, remind customers who previously enquired the tour but did not sign up.
- Remind customers to pay outstanding fee 3 days before the tour