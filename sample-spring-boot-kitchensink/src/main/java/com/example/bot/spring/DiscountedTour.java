package com.example.bot.spring;

import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
/** A class to hold details of tour with discount
 * @see Tour*/
@Slf4j
public class DiscountedTour extends Tour {
	/** The given discount */
	private static final double DISCOUNT = 0.8;
	/** Format of date: yyyy-MM-dd */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/** Constructor. It calls the constructor of its SuperClass. It sets the price with
	 * with discounts and set the corresponding date and capacity.
	 * @param name tour name
	 * @param tourID tour ID
	 * @param description tour description
	 * @param weekendPrice the price of the tour in weekend
	 * @param weekdayPrice the price of the tour in weekday
	 * @param days duration of tour
	 * @param capacity places left for the tour on the specific date
	 * @param date tour date*/
	public DiscountedTour(String name, String tourID, String description, double weekendPrice, double weekdayPrice, int days, int capacity, Date date) {
		super(name, tourID, description, weekendPrice, weekdayPrice, days);
		setWeekdayPrice(weekdayPrice * DISCOUNT);
		setWeekendPrice(weekendPrice * DISCOUNT);
		setAvailableDate(date);
		setCapacity(capacity);
	}
	
	/** This method returns the details of tour in String, using ':' to separate.
	 * @return String representation of discounted tour*/
	public String toString() {
		String date = dateFormat.format(getAvailableDate().get(0));
		int capacity = getCapacity().get(0);
		return getName() + ":" + getTourID() + ":" + getDescription() + ":" + getWeekendPrice() + ":" + getWeekdayPrice() + ":" + getDays() + ":" + capacity + ":" + date;
	}
	
	/** Get discount
	 * @return discount
	 * @see #DISCOUNT*/
	public double getDiscount() { return DISCOUNT; }
}
