package com.example.bot.spring;

import java.util.ArrayList;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import lombok.extern.slf4j.Slf4j;

/** A class retrieving booking records of given customer(if any) from the database
 * and push them to that customer.
 * @see FunctionStrategy*/
@Slf4j
public class SearchBookingRecord implements FunctionStrategy {

	/** Database engine */
	private SQLDatabaseEngine database = new SQLDatabaseEngine();
	/** String[] type arraylist to store the searched booking record*/
	private ArrayList<String[]> recordArr;
	/** For listing booking records if number of records is larger than 4 */
	private int recordListIndex;
	
	/** This method asks customer for personal details, verify the details and
	 * if the details are correct, customer details will be updated in database.
	 * This function is called when customer wants to book tour and he/she has no
	 * record in the database.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer */
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception{

		ArrayList<Message> messages = new ArrayList<Message>();
		recordArr = database.searchBookingRecord(customer.getLineID());
		recordListIndex = customer.getRecordListIndex();

		if (recordArr.size() == 0) {
			customer.setStatus(-2);
			TextMessage message = new TextMessage("You do not have any booking record.");
			messages.add(message);
			return messages;
		}

		
		if (tokens[0].equals("yes")) {
			return displayMoreRecord(customer, messages);
		}

		if (tokens[0].equals("no")) {
			TextMessage message = new TextMessage("What other help do you need?");
			messages.add(message);
			customer.setRecordListIndex(-1);
			customer.setStatus(-2);
			return messages;
		}

		if (recordArr.size() <= 4) {
			TextMessage message1 = new TextMessage("You have " + recordArr.size() + " booking record(s).");
			messages.add(message1);
			for(int i = 0; i < recordArr.size(); i++) {
				String[] temp = recordArr.get(i);
				String line1 = String.valueOf(i+1) + ". Tour: (" + temp[0] + ") " + temp[1] + " - " + temp[2];
				String line2 = "\n Number of adults: " + temp[3];
				String line3 = "\n Number of children: " + temp[4];
				String line4 = "\n Number of toddler: " + temp[5];
				String line5 = "\n Total fee: " + temp[6];
				String line6 = "\n Amount paid: " + temp[7];
				String line7 = "\n Special request: " + temp[8];
				TextMessage message2 = new TextMessage(line1 + line2 + line3 + line4 + line5 + line6 + line7);
				messages.add(message2);
			}
			customer.setStatus(-2);
			return messages;
		}
		else if (recordArr.size() > 4 && recordListIndex == -1) {

			return displayMoreRecord(customer, messages);
		}

		//error message
		TextMessage message = new TextMessage("Sorry I don't understand what you are saying.");
		messages.add(message);
		return messages;
	}

	/** This method asks customer for personal details, verify the details and
	 * if the details are correct, customer details will be updated in database.
	 * This function is called when customer wants to see more records
	 * @param text the customer input
	 * @param List of messages to be replied to customer
	 * @return List of messages to reply to customer */
	private ArrayList<Message> displayMoreRecord(Customer customer, ArrayList<Message> messages) {
		for(int i = recordListIndex + 1; i < recordArr.size(); i++) {
			if (i == 0) {
				TextMessage message1 = new TextMessage("You have " + recordArr.size() + " booking record(s).");
				messages.add(message1);
			}

			String[] temp = recordArr.get(i);
			String line1 = String.valueOf(i+1) + ". Tour: (" + temp[0] + ") " + temp[1] + " - " + temp[2];
			String line2 = "\n Number of adults: " + temp[3];
			String line3 = "\n Number of children: " + temp[4];
			String line4 = "\n Number of toddler: " + temp[5];
			String line5 = "\n Total fee: " + temp[6];
			String line6 = "\n Amount paid: " + temp[7];
			String line7 = "\n Special request: " + temp[8];
			TextMessage message2 = new TextMessage(line1 + line2 + line3 + line4 + line5 + line6 + line7);
			messages.add(message2);

			if ((i + 2) % 4 == 0 && (i + 1) != recordArr.size()) {
				TextMessage continueMsg = new TextMessage("Do you wish to list the remaining booking record(s)? (Yes/No)");
				messages.add(continueMsg);
				customer.setRecordListIndex(i);
				break;
			}

			if((i + 1) == recordArr.size()) {
				customer.setRecordListIndex(-1);
				customer.setStatus(-2);
			}
		}

		return messages;
	}

}
