package com.example.bot.spring;

import java.util.ArrayList;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import lombok.extern.slf4j.Slf4j;

/** A class retrieving saved tours for given customer(if any) from the database
 * and push them to that customer.
 * @see FunctionStrategy*/
@Slf4j
public class SearchSavedTour implements FunctionStrategy {

	/** Database engine */
	private SQLDatabaseEngine database = new SQLDatabaseEngine();
	/** String[] type arraylist to store all the saved tour of a customer */
	private ArrayList<String[]> savedTourArr;
    /** For listing saved tours if number of saved tours is larger than 4 */
	private int savedListIndex;
	
	/** This method asks customer for personal details, verify the details and
	 * if the details are correct, customer details will be updated in database.
	 * This function is called when customer wants to book tour and he/she has no
	 * record in the database.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer */
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) throws Exception {

		ArrayList<Message> messages = new ArrayList<Message>();
		savedTourArr = database.getSavedTour(customer.getCustomerID());
		savedListIndex = customer.getSavedListIndex();

		if (savedTourArr.size() == 0) {
			customer.setStatus(-2);
			TextMessage message = new TextMessage("You do not have any saved tours.");
			messages.add(message);
			return messages;
		}

		if (tokens[0].equals("yes")) {
			return displayMoreRecord(customer, messages);
		}

		if (tokens[0].equals("no")) {
			TextMessage message = new TextMessage("What other help do you need?");
			messages.add(message);
			customer.setSavedListIndex(-1);
			customer.setStatus(-2);
			return messages;
		}

		if (savedTourArr.size() <= 4) {
			TextMessage message1 = new TextMessage("You have " + savedTourArr.size() + " saved tour(s).");
			messages.add(message1);
			for(int i = 0; i < savedTourArr.size(); i++) {
				String[] temp = savedTourArr.get(i);
				TextMessage message2 = new TextMessage(String.valueOf(i+1) + ". Tour: (" + temp[0] + ") " + temp[1] + " is saved on " + temp[2]);
				messages.add(message2);
			}
			customer.setStatus(-2);
			return messages;
		}
		else if (savedTourArr.size() > 4 && savedListIndex == -1) {

			return displayMoreRecord(customer, messages);
		}

		//error message
		TextMessage message = new TextMessage("Sorry I don't understand what you are saying.");
		messages.add(message);
		return messages;
	}

	/** This method asks customer for personal details, verify the details and
	 * if the details are correct, customer details will be updated in database.
	 * This function is called when customer wants to see more records
	 * @param text the customer input
	 * @param List of messages to be replied to customer
	 * @return List of messages to reply to customer */
	private ArrayList<Message> displayMoreRecord(Customer customer, ArrayList<Message> messages) {
		for(int i = savedListIndex + 1; i < savedTourArr.size(); i++) {
			if (i == 0) {
				TextMessage message1 = new TextMessage("You have " + savedTourArr.size() + " saved tour(s).");
				messages.add(message1);
			}

			String[] temp = savedTourArr.get(i);
			TextMessage message2 = new TextMessage(String.valueOf(i+1) + ". Tour: (" + temp[0] + ") " + temp[1] + " is saved on " + temp[2]);
			messages.add(message2);

			if ((i + 2) % 4 == 0 && (i + 1) != savedTourArr.size()) {
				TextMessage continueMsg = new TextMessage("Do you wish to list the remaining saved tour(s)? (Yes/No)");
				messages.add(continueMsg);
				customer.setSavedListIndex(i);
				break;
			}

			if((i + 1) == savedTourArr.size()) {
				customer.setSavedListIndex(-1);
				customer.setStatus(-2);
			}
		}

		return messages;
	}

}