package com.example.bot.spring;

import java.util.ArrayList;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import lombok.extern.slf4j.Slf4j;

/** A class handling exit of customer
 * @see FunctionStrategy*/
@Slf4j
public class Exit implements FunctionStrategy {
	
	/** This method reply customer politely with GoodBye.
	 * @param text the customer input
	 * @param tokens all English words from text by splitting it using opennlp
	 * @param tags part of speech of each token
	 * @param customer the customer that is booking tour
	 * @return List of messages to reply to customer*/
	@Override
	public ArrayList<Message> functionOperation(String text, String[] tokens, String[] tags, Customer customer) {
		ArrayList<Message> messages = new ArrayList<Message>();
		TextMessage message = new TextMessage("Thank you for using our service. Bye!");
		messages.add(message);
		return messages;
	}
}