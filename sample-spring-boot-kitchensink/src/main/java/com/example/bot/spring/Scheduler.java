package com.example.bot.spring;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.Observable;
import java.util.ArrayList;


import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.model.message.template.ButtonsTemplate;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.action.Action;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import lombok.NonNull;

/** A class handling all scheduled tasks for pushing messages to customers
 *  
 * */

@Slf4j
@Component
public class Scheduler{
	/**
	 * The only object of this class to be created
	 */
	private static Scheduler instance = new Scheduler();
	
	/**
	 * The Line Messaging Client to push messages
	 * 
	 * @see #sendMessage(String, List)
	 */
	@Autowired
	private LineMessagingClient lineMessagingClient;
	
	/**
	 * The database object
	 */
	private SQLDatabaseEngine database = new SQLDatabaseEngine();
	/**
	 * The list of customers' line IDs who have added this chatbot as
	 * friend
	 * @see #pushMsgForDiscount()
	 * @see #pushMsgForNewTour()
	 * @see #pushMsgForRecommendation()
	 * @see #pushMsgOnReminderOutstandingFee()
	 * @see #pushMsgOnReminderSavedTour()
	 * @see #pushMsgOnReminderUpcomingTour()
	 */
	private ArrayList<String> lineIDs;
	
	//private static final org.slf4j.Logger log = LoggerFactory.getLogger(Scheduler.class);
	/**
	 * The calendar to get current date and time
	 * @see #pushMsgForDiscount()
	 * @see #pushMsgForNewTour()
	 * @see #pushMsgForRecommendation()
	 * @see #pushMsgOnReminderOutstandingFee()
	 * @see #pushMsgOnReminderSavedTour()
	 * @see #pushMsgOnReminderUpcomingTour()
	 * */
	private Calendar cal;	
	/**
	 * The hour of day to remind customers with their upcoming tours, 
	 * outstanding fees of their booked tours, and their saved tours
	 * @see #pushMsgOnReminderOutstandingFee()
	 * @see #pushMsgOnReminderSavedTour()
	 * @see #pushMsgOnReminderUpcomingTour()
	 */

	private int scheduleHour = 20;
	/**
	 * The minute to remind customers with their upcoming tours, 
	 * outstanding fees of their booked tours, and their saved tours
	 * @see #pushMsgOnReminderOutstandingFee()
	 * @see #pushMsgOnReminderSavedTour()
	 * @see #pushMsgOnReminderUpcomingTour()
	 */
	private int scheduleMinute = 35;

    //private int msg_counter = 0;
	
    /**
     * This method is used to ensure no constructor can be called outside this
     * class.
     */
    private Scheduler() {}

	/**
	 * This method is used to ensure only one object can be created
     * for this class.
     * @return the only instance of this class.
	 */
	public static Scheduler getInstance() {
		return instance;
	}
	
	/**
	 * This method runs every 59s and push messages to clients to remind them 
	 * the upcoming tours if any.
	 * */
	@Scheduled(fixedDelay=59000)
	public void pushMsgOnReminderUpcomingTour() {
        //log.info("Execute every 59 seconds! Scheduled Reminder of Upcoming Tour.");

		cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Hong_Kong"));
		if (cal.get(Calendar.HOUR_OF_DAY) == scheduleHour && cal.get(Calendar.MINUTE) == scheduleMinute) {
			try {
				ArrayList<String[]> result = new ArrayList<String[]>();
				result = database.getUpcomingTour();

				if (result.size() != 0 && result != null) {
					for (int i = 0; i < result.size(); i++) {
						String[] temp = result.get(i);
						String msg = "Don't forget! " + temp[1] + " is departed on " + temp[2] + "!";

						sendMessage(temp[0], msg);
					}
				} else {
					log.info("Upcoming Tour: Not found");
				}
			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
	}

	/**
	 * This method runs every 59s and push messages to clients to remind them 
	 * to pay the outstanding fees of their booked tours if any.
	 * */
	@Scheduled(fixedDelay=59000)
	public void pushMsgOnReminderOutstandingFee() {
        //log.info("Execute every 59 seconds! Scheduled Reminder of Outstanding Fee.");

		cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Hong_Kong"));
		if (cal.get(Calendar.HOUR_OF_DAY) == scheduleHour && cal.get(Calendar.MINUTE) == scheduleMinute) {
			try {
				ArrayList<String[]> result = new ArrayList<String[]>();
				result = database.getOutstandingFee();

				if (result.size() != 0) {
					for (int i = 0; i < result.size(); i++) {
						String[] temp = result.get(i);
						String msg = "Please pay outstanding fee of $" + temp[3] + " before " + temp[2] + " for " + temp[1] + ".";

						sendMessage(temp[0], msg);
					}
				} else {
					log.info("Outstanding Fee: Not found");
				}

			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
	}

	/**
	 * This method runs every 59s and push messages to clients to remind them 
	 * the tours they saved before if any.
	 * */
	@Scheduled(fixedDelay=59000)
	public void pushMsgOnReminderSavedTour() {
        //log.info("Execute every 59 seconds! Scheduled Reminder of Saved Tour.");

		cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Hong_Kong"));
		if (cal.get(Calendar.HOUR_OF_DAY) == scheduleHour && cal.get(Calendar.MINUTE) == scheduleMinute) {
			try {
				ArrayList<String[]> result = new ArrayList<String[]>();
				result = database.getLatestSavedTour();

				if (result.size() != 0) {
					for (int i = 0; i < result.size(); i++) {
						String[] temp = result.get(i);
						String msg = "You saved one tour on " + temp[1] + ".\n" + temp[2] + " " + temp[3] + ".";

                        // if (temp[0].equals("U53bf0b36e6e4af47a6f51c23392e5f88"))
						sendMessage(temp[0], msg);
					}
				} else {
					log.info("Saved Tour: Not found");
				}
			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
	}
	
	/**
	 * This method runs every 59s and push messages to clients to recommend
	 * tours for them based on their previous booking records at 4pm every day. 
	 * If they have not booked anything before, random tours will be recommended.
	 * */
	@Scheduled(fixedDelay=59000)
	public void pushMsgForRecommendation() {
		cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Hong_Kong"));
		if (cal.get(Calendar.HOUR_OF_DAY) == 16 && cal.get(Calendar.MINUTE) == 0) {
			try {
				// Get lineID from database
				lineIDs = database.getExistingLineID();
				// Check if there is any recommend tour
				for (String lineID: lineIDs) {
					ArrayList<String> recommendTours = database.recommendTour(lineID);
					if (recommendTours != null && recommendTours.size() != 0) {
						log.info("Recommend Tours found!");
						ArrayList<Message> messages = new ArrayList<Message>();
						for (String tourName : recommendTours) {
							String promotionMsg = tourName + " is now available. "
							+ "We thought you may be interested in it. Search now to know more!";
							TextMessage message = new TextMessage(promotionMsg);
							messages.add(message);
						}
						sendMessage(lineID, messages);
					}
				}
				
			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
	}
	
	/**
	 * This method runs every 59s and push messages to clients to inform them
	 * with the newly opened tours if any at 12pm every day.
	 * */
	@Scheduled(fixedDelay = 59000)
	public void pushMsgForNewTour() {
		cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Hong_Kong"));
		if (cal.get(Calendar.HOUR_OF_DAY) == 12 && cal.get(Calendar.MINUTE) == 0) {
			try {
				// Check if there is any newly opened tour
				ArrayList<String[]> newTour = database.findNewTour();
				if (newTour.size() > 0) {
					log.info("New Tours Found!");
					// Get lineID from database
					lineIDs = database.getExistingLineID();
					// Notify customers the newly opened tour
					for (String lineID: lineIDs) {
						ArrayList<Message> messages = new ArrayList<Message>();
						for (String[] tour: newTour) {
							String promotionMsg = tour[0] + " on " + tour[1] + " is now available! "
							+ "Search to know more!";
							TextMessage message = new TextMessage(promotionMsg);
							messages.add(message);
						}
						sendMessage(lineID, messages);
					}
				}
				
			} catch (Exception e) {
				log.info(e.getMessage());
			}
		}
	}
	
	/**
	 * This method runs every 59s and push messages to clients to inform them
	 * with the new promotion campaign (e.g. double 11 campaign) if any.
	 * 
	 * */
	@Scheduled(fixedDelay=59000)
	public void pushMsgForDiscount() {
		cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Hong_Kong"));
		
		if (cal.get(Calendar.HOUR_OF_DAY) == 18 && cal.get(Calendar.MINUTE) == 30) {
			try {
				// Get lineID from database
				lineIDs = database.getExistingLineID();
				// Check if there is any tour with discount
				DiscountedTour discountedTour = database.checkTourDiscount();
				if (discountedTour != null) {
					log.info("Discounted Tour found!");
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
					String date = dateFormat.format(discountedTour.getAvailableDate().get(0));
					// Display discount in correct format
					double discount = discountedTour.getDiscount() * 100;
					String promotionMsg = "";
					if ((int) discount == discount) {
						int intDiscount = (int) discount;
						// Set promotion message
						promotionMsg = discountedTour.getName() + " on " + date + " is having an " 
						+ intDiscount + "% discount now!!!\n"
						+ "Every customer can reserve at most 2 tickets! There are only few places left. Act Now!";
					} else {
						// Set promotion message
						promotionMsg = discountedTour.getName() + " on " + date + " is having an " 
						+ discount + "% discount now!!!\n"
						+ "Every customer can reserve at most 2 tickets! There are only few places left. Act Now!";
					}
					
					// Create messages with postback action
					PostbackAction msgAction = new PostbackAction("Book now", discountedTour.toString());
					ArrayList<Action> listAction = new ArrayList<Action>();
					listAction.add(msgAction);
					ButtonsTemplate template = new ButtonsTemplate(null, null, promotionMsg, listAction);
					TemplateMessage msg = new TemplateMessage(promotionMsg, template);
					
					// Push message to all clients
					for (String s: lineIDs) {
						sendMessage(s, msg);
					}
				}
			} catch (Exception e) {
				StringWriter writer = new StringWriter();
				e.printStackTrace(new PrintWriter(writer));
				String s = writer.toString();
				log.info(s);
			}
		}
	}
	
//    @Scheduled(fixedDelay=1800000)
//    public void timeOutTest() {
//            log.info("Execute every 30 minutes!");
//
//            msg_counter++;
//            String msg = "Time-out test No." + msg_counter;
//
//            sendMessage("U53bf0b36e6e4af47a6f51c23392e5f88", msg);
//    }
	
	/**
	 * This method calls pushMessage function and push the messages to the 
	 * Line ID.
	 * 
	 * @param lineID This is the Line ID of the targeted client. It cannot be null.
	 * @param messages This is the list of messages needed to be sent. It cannot be null.
	 * 
	 * @see com.linecorp.bot.client.LineMessagingClient#pushMessage(PushMessage)
	 * */
	private void sendMessage(@NonNull String lineID, @NonNull List<Message> messages) {
		try {
			BotApiResponse apiResponse = lineMessagingClient.pushMessage(new PushMessage(lineID, messages)).get();
			log.info("Sent messages: {}", apiResponse);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * This method override the sendMessage function. It creates a singletonList for
	 * one message and call the sendMessage function.
	 * 
	 * @param lineID This is the Line ID of the targeted client. It cannot be null.
	 * @param messages This is the list of messages needed to be sent. It cannot be null.
	 * 
	 * @see #sendMessage(String, List)
	 * */
	private void sendMessage(@NonNull String lineID, @NonNull Message message) {
		sendMessage(lineID, Collections.singletonList(message));
	}
	/**
	 * This method override the sendMessage function. It restrains the number of 
	 * characters in the message to be less than 1000. If there are more than 
	 * 1000 characters, it will convert the message to show the first 998 characters
	 * and append ".." at the end. It will then call sendMessage function to push the
	 * message.
	 * 
	 * @param lineID This is the Line ID of the targeted client. It cannot be null.
	 * @param messages This is the list of messages needed to be sent. It cannot be null.
	 * 
	 * @see #sendMessage(String, Message)
	 * */
	private void sendMessage(@NonNull String lineID, @NonNull String message) {
		if (message.length() > 1000) {
			message = message.substring(0, 1000 - 2) + "..";
		}
		sendMessage(lineID, new TextMessage(message));
	}
}